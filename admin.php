<?php
	include 'functions.php';
	session_start();

	// $connectionData['HOST'] = '188.120.233.65';
	// $connectionData['USER'] = 'telegram';
	// $connectionData['PASSWORD'] = 'telegram12345';
	// $connectionData['BASE'] = 'phoneAdvisor';

	$connectionData = $_SESSION['connectionData'];
	$db = new mysqli($connectionData['HOST'], $connectionData['USER'], $connectionData['PASSWORD'], 'telegram', "3306");
	$db->set_charset("utf8");


	if (isset($_POST['Logout']) and $_POST['Logout'] != null) {
		unset($_COOKIE['AUTH_TOKEN']);
		$_SESSION['User'] = null;
		header('Location: /login.php');
	}

	if (!isset($_SESSION['User']) and !isset($_COOKIE["AUTH_TOKEN"])) {
		header('Location: /login.php');
		die();
	}

	if (isset($_GET['getBrands'])) {
		$brands = select($db, '*', 'Brands', NULL);
		echo json_encode($brands); die();
	}

	if (isset($_GET['getModels'])) {
		if ($_GET['getModels'] == 'GET') {
			$brands = select($db, '*', 'Brands', NULL);
			$brandsArr = [];
			$modelsArr = [];
			$finalArr = [];
			foreach ($brands as $k => $v) {
				$brandsArr[$v['Id']] = $v['Brand'];
				$models = select($db, '*', 'Models', 'Brand=\'' . $v['Id'] . '\'');
				foreach ($models as $n => $m) {
					$modelsArr[$m['Id']] = ['Brand' => $v['Id'], 'Model' => $m['Model']];
				}
			}
			$finalArr['Brands'] = $brandsArr;
			$finalArr['Models'] = $modelsArr;
			echo json_encode($finalArr); die();
		} else {
			$brand = select($db, 'Id', 'Brands', 'Brand=\'' . $_GET['getModels'] . '\'')[0]['Id'];
			$models = select($db, '*', 'Models', 'Brand=\'' . $brand . '\'');
			echo json_encode($models); die();
		}
	}

	if (isset($_GET['updateUser'])) {
		$user = select($db, '*', 'Users', 'Username=\'' . $_GET['updateUser'] . '\'', NULL);
		if (empty($user)) {
			$username = $_GET['updateUser'];
			$brand = select($db, 'Id', 'Brands', 'Brand=\'' . $_GET['brand'] . '\'')[0]['Id'];
			$model = select($db, 'Id', 'Models', 'Model=\'' . $_GET['model'] . '\'')[0]['Id'];
			insert($db, 'Users', 'Username, Brand, Model', '\'' . $username . '\', \'' . $brand . '\', \'' . $model . '\'');
		} else {
			$username = $user[0]['Username'];
			$brand = select($db, 'Id', 'Brands', 'Brand=\'' . $_GET['brand'] . '\'')[0]['Id'];
			$model = select($db, 'Id', 'Models', 'Model=\'' . $_GET['model'] . '\'')[0]['Id'];
			update($db, 'Users', 'Brand=\'' . $brand . '\'' . ', Model=\'' . $model . '\'', 'Username=\'' . $username . '\'');
		}
		// echo $user; die();
	}

	if (isset($_GET['getCategories'])) {
		$categories = select($db, '*', 'Categories', NULL);
		echo json_encode($categories); die();
	}

	if (isset($_GET['getUsersPhone'])) {
		$phone = array();
		$brand = select($db, 'Brand', 'Users', 'Username=\'' . $_GET['getUsersPhone'] . '\'');
		$model = select($db, 'Model', 'Users', 'Username=\'' . $_GET['getUsersPhone'] . '\'');
		array_push($phone, $brand);
		array_push($phone, $model);
		echo json_encode($phone); die();
	}

	if (isset($_GET['getInstructions']) and $_GET['getInstructions'] == 'GET') {
		$instructions = select($db, '*', 'Instruction', NULL);
		die(json_encode($instructions));
	} else if (isset($_GET['getInstructions'])) {
		$brand = $_GET['brand'];
		$categoryName = $_GET['getInstructions'];
		$category = select($db, 'Id', 'Categories', 'Name=\'' . $categoryName . '\'')[0]['Id'];
		$instructions = array();
		if (isset($_GET['model'])) {
			$model = $_GET['model'];
			$instruction = select($db, 'Link, Header', 'Instruction', 'Brand=\'' . $brand . '\' AND Model=\'' . $model . '\' AND Category=\'' . $category . '\'');
		} else {
			$instruction = select($db, 'Link, Header', 'Instruction', 'Brand=\'' . $brand . '\' AND Category=\'' . $category . '\'');
		}
		foreach ($instruction as $k => $v) {
			$instructions[$v['Header']] = $v['Link'];
		}
		echo json_encode($instructions); die();
	}

	if (isset($_GET['getSongs'])) {
		$songs = select($db, '*', 'Chart', NULL);
		echo json_encode($songs); die();
	}

	if (isset($_POST['likeSong'])) {
		$song = $_POST['likeSong'];
		$username = $_POST['username'];
		update($db, 'Chart', 'Likes = Likes + 1', 'Id=\'' . $song . '\'');
		update($db, 'Users', 'VotedFor = \'' . $song . '\'', 'Username=\'' . $username . '\'');
		die('Thanks for voting');
	}

	if (isset($_GET['showChart'])) {
		$chart = select($db, '*', 'Chart', NULL);
		echo json_encode($chart); die();
	}

	if (isset($_GET['VotedFor'])) {
		$username = $_GET['VotedFor'];
		$vote = select($db, 'VotedFor', 'Users', 'Username=\'' . $username . '\'');
		echo json_encode($vote[0]['VotedFor']); die();
	}

	if (isset($_GET['getDataForApi'])) {
		$data = array();
		$brands = select($db, '*', 'Brands', NULL);
		$models = select($db, '*', 'Models', NULL);
		$categories = select($db, '*', 'Categories', NULL);
		$instructions = select($db, '*', 'Instruction', NULL);

		$data['Brands'] = $brands;
		$data['Models'] = $models;
		$data['Categories'] = $categories;
		$data['Instructions'] = $instructions;
		echo json_encode($data); die();
	}

	if (isset($_POST['updateInstruction']) and $_POST['updateInstruction'] == 'Update') {
		$id = $_POST['id'];
		$url = quotemeta($_POST['url']);
		$header = $_POST['header'];
		$brand = $_POST['brand'];
		$model = $_POST['model'];
		$category = $_POST['category'];

		$isNew = select($db, '*', 'Instruction', 'Id=\''.$id.'\'');

		if ($isNew == []) {
			if ($model == '') {
				insert($db, 'Instruction', 'Id, Link, Header, Brand, Category', '\''.$id.'\', \''.$url.'\', \''.$header.'\', \''.$brand.'\', \''.$category.'\'');
			} else {
				insert($db, 'Instruction', 'Id, Link, Header, Brand, Model, Category', '\''.$id.'\', \''.$url.'\', \''.$header.'\', \''.$brand.'\', \''.$model.'\', \''.$category.'\'');
			}
		} else {
			if ($model == '') {
				update($db, 'Instruction', 'Link=\''.$url.'\', Header=\''.$header.'\', Brand=\''.$brand.'\', Category=\''.$category.'\'', 'Id=\''.$id.'\'');
			} else {
				update($db, 'Instruction', 'Link=\''.$url.'\', Header=\''.$header.'\', Brand=\''.$brand.'\', Model=\''.$model.'\', Category=\''.$category.'\'', 'Id=\''.$id.'\'');
			}
		}

		
		echo 'Success'; die();
	}

	if (isset($_POST['updateInstruction']) and $_POST['updateInstruction'] == 'Delete') {
		$id = $_POST['id'];
		delete($db, 'Instruction', 'Id=\''.$id.'\'');
		die('Success');
	}

	if (isset($_POST['updateChart']) and $_POST['updateChart'] == 'Update') {
		$id = $_POST['id'];
		$singer = $_POST['singer'];
		$song = $_POST['song'];
		$likes = $_POST['likes'];
		
		$isNew = select($db, '*', 'Chart', 'Id=\''.$id.'\'');
		if ($isNew == []) {
			insert($db, 'Chart', 'Id, Singer, Song, Likes', '\''.$id.'\', \''.$singer.'\', \''.$song.'\', \''.$likes.'\'');
		} else {
			update($db, 'Chart', 'Singer=\''.$singer.'\', Song=\''.$song.'\', Likes=\''.$likes.'\'', 'Id=\''.$id.'\'');
		}

		die('Success');
	}

	if (isset($_POST['updateChart']) and $_POST['updateChart'] == 'Delete') {
		$id = $_POST['id'];
		delete($db, 'Chart', 'Id=\''.$id.'\'');
		die('Success');
	}

	if (isset($_POST['updateModel']) and $_POST['updateModel'] == 'Update') {
		$id = $_POST['id'];
		$brand = $_POST['brand'];
		$model = $_POST['model'];

		$isNewBrand = select($db, '*', 'Brands', 'Brand=\''.$brand.'\'');
		if ($isNewBrand == []) {
			insert($db, 'Brands', 'Brand', '\''.$brand.'\'');
		}

		$brand = select($db, 'Id', 'Brands', 'Brand=\'' . $_POST['brand'] . '\'')[0]['Id'];

		$isNewModel = select($db, '*', 'Models', 'Id=\''.$id.'\'');
		if ($isNewModel == []) {
			insert($db, 'Models', 'Id, Brand, Model', '\''.$id.'\', \''.$brand.'\', \''.$model.'\'');
		} else {
			update($db, 'Models', 'Brand=\''.$brand.'\', Model=\''.$model.'\'', 'Id=\''.$id.'\'');
		}

		die('Success');
	}

	if (isset($_POST['updateModel']) and $_POST['updateModel'] == 'Delete') {
		$id = $_POST['id'];
		delete($db, 'Models', 'Id=\''.$id.'\'');
		die('Success');
	}

	if (isset($_GET['getUser']) and $_GET['getUser'] == 'GET') {
		echo json_encode($_SESSION['User']);
		die();
	}

	function getDayById($days, $id) {
		foreach ($days as $k => $v) {
			if ($v['Id'] == $id)
				return $v;
		}
	}

	function getUserById($users, $id) {
		foreach ($users as $k => $v) {
			if ($v['Id'] == $id)
				return $v;
		}
	}

	// function insert($db, $table, $fields, $values) {
	// 	$query = 'INSERT INTO ' . $table . ' (' . $fields . ') VALUES (' . $values . ')';
	// 	// echo $query; die();
	// 	$queryResult = $db->query($query);
	// 	return $queryResult;
	// }

	// function update($db, $table, $set, $where) {
	// 	$query = 'UPDATE ' . $table . ' SET ' . $set . ' WHERE ' . $where;
	// 	echo $query; die();
	// 	$queryResult = $db->query($query);
	// 	return $queryResult;
	// }

	// function select($db, $what, $where, $condition) {
	// 	if (isset($condition)) 
	// 		$query = 'SELECT ' . $what . ' FROM ' . $where . ' WHERE ' . $condition;
	// 	else
	// 		$query = 'SELECT ' . $what . ' FROM ' . $where;
	// 	// echo $query; die();
	// 	$queryResult = $db->query($query);
	// 	$result = array();
	// 	while ($row = $queryResult->fetch_assoc()) {
	// 		array_push($result, $row);
	// 	}
	// 	return $result;
	// }

	// function delete($db, $from, $where) {
	// 	$query = 'DELETE FROM ' . $from . ' WHERE ' . $where;
	// 	$queryResult = $db->query($query);
	// 	return $queryResult;
	// }
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">

	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="ThemeBucket">
	<link rel="shortcut icon" href="template/images/favicon.png">

	<title>Admin Panel</title>

	<!--Core CSS -->
	<link href="template/bs3/css/bootstrap.min.css" rel="stylesheet">
	<link href="template/css/bootstrap-reset.css" rel="stylesheet">
	<link href="template/font-awesome/css/font-awesome.css" rel="stylesheet" />

	<link rel="stylesheet" href="template/js/data-tables/DT_bootstrap.css" />

	<!-- Custom styles for this template -->
	<link href="template/css/style.css" rel="stylesheet">
	<link href="template/css/style-responsive.css" rel="stylesheet" />

	<!-- Just for debugging purposes. Don't actually copy this line! -->
	<!--[if lt IE 9]>
	<script src="js/ie8-responsive-file-warning.js"></script><![endif]-->

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
	<script src="/template/js/jquery.min.js" type="text/javascript"></script>

	<script type="text/javascript">
		$(document).ready(function(){
			function getObjects(obj, key, val) {
				var objects = [];
				for (var i in obj) {
					if (!obj.hasOwnProperty(i)) continue;
					if (typeof obj[i] == 'object') {
						objects = objects.concat(getObjects(obj[i], key, val));
					} else if (i == key && obj[key] == val) {
						objects.push(obj);
					}
				}
				return objects;
			}

			var getUrlParameter = function getUrlParameter(sParam) {
				var sPageURL = decodeURIComponent(window.location.search.substring(1)),
					sURLVariables = sPageURL.split('&'),
					sParameterName,
					i;

				for (i = 0; i < sURLVariables.length; i++) {
					sParameterName = sURLVariables[i].split('=');

					if (sParameterName[0] === sParam) {
						return sParameterName[1] === undefined ? true : sParameterName[1];
					}
				}
			};
			var currentUserData;
			var loadUserData = $.getJSON("index.php/?getUserData=GET").done(function(data){
				currentUserData = data;
				console.log(data);
				var botSubmenu = $('.bots-submenu');
				$('.username').text(data['Username']);
				$.each(data['Bots'], function(key, value) {
					botSubmenu.append("<li bot=\"" + value['Username'] + "\"><a href=\"bot.php?bot=" + value['Username'] + "\">" + value['Name'] + "</a></li>");
				});

				var menu = $('.sidebar-menu');
            	var submenu = '<li class="sub-menu"><a data-toggle="collapse" href=".admin-submenu"><i class="fa fa-laptop"></i><span>Admin Info</span></a><ul class="sub admin-submenu collapse"></ul></li>';
            	menu.append(submenu);
            	var adminSubmenu = $('.admin-submenu');
            	$.each(data['AdminInfo'], function (k, v) {
            		adminSubmenu.append("<li admin-info=\"" + k + "\"><a href=\"admin.php?info=" + k + "\">" + k + "</a></li>");
            	});


				var infoTable = $('.info-table');
				var headerRow = $('<tr></tr>').appendTo(infoTable);
				var info = getUrlParameter('info');
				if (info == 'Brands') {
					$('<th>Id</th>').appendTo(headerRow);
					$('<th>Brand</th>').appendTo(headerRow);
					$('<th>Edit</th>').appendTo(headerRow);
					$('<th>Delete</th>').appendTo(headerRow);
					$('<th>Apply</th>').appendTo(headerRow);
					// console.log(data['AdminInfo'][info]);
					$.each(data['AdminInfo'][info], function (k, v) {
						// console.log(v);
						var newRow = $('<tr id="' + v['Id'] + '"></tr>').appendTo(infoTable);
						var idCell = $('<td>' + v['Id'] + '</td>').appendTo(newRow);
						var brandCell = $('<td class="brand-name">' + v['Brand'] + '</td>').appendTo(newRow);
						var editCell = $('<td><a class="edit-brand">Edit</a></td>').appendTo(newRow);
						var deleteCell = $('<td><a class="delete-brand">Delete</a></td>').appendTo(newRow);
						var applyCell = $('<td><a class="apply-brand">Apply</a></td>').appendTo(newRow);

					});
				} else if (info == 'Models') {
					$('<th>Id</th>').appendTo(headerRow);
					$('<th>Brand</th>').appendTo(headerRow);
					$('<th>Model</th>').appendTo(headerRow);
					$('<th>Edit</th>').appendTo(headerRow);
					$('<th>Delete</th>').appendTo(headerRow);
					$('<th>Apply</th>').appendTo(headerRow);

					$.each(data['AdminInfo'][info], function (k, v) {
						// console.log(getObjects(data['AdminInfo']['Brands'], 'Id', v['Brand']));
						var newRow = $('<tr id="' + v['Id'] + '"></tr>').appendTo(infoTable);
						var idCell = $('<td>' + v['Id'] + '</td>').appendTo(newRow);
						var brandCell = $('<td class="model-brand">' + getObjects(data['AdminInfo']['Brands'], 'Id', v['Brand'])[0]['Brand'] + '</td>').appendTo(newRow);
						var modelCell = $('<td class="model-name">' + v['Model'] + '</td>').appendTo(newRow);
						var editCell = $('<td><a class="edit-model">Edit</a></td>').appendTo(newRow);
						var deleteCell = $('<td><a class="delete-model">Delete</a></td>').appendTo(newRow);
						var applyCell = $('<td><a class="apply-model">Apply</a></td>').appendTo(newRow);
					});
				} else if (info == 'Categories') {
					$('<th>Id</th>').appendTo(headerRow);
					$('<th>Link</th>').appendTo(headerRow);
					$('<th>Edit</th>').appendTo(headerRow);
					$('<th>Delete</th>').appendTo(headerRow);
					$('<th>Apply</th>').appendTo(headerRow);
					// console.log(data['AdminInfo'][info]);
					$.each(data['AdminInfo'][info], function (k, v) {
						// console.log(v);
						var newRow = $('<tr id="' + v['Id'] + '"></tr>').appendTo(infoTable);
						var idCell = $('<td>' + v['Id'] + '</td>').appendTo(newRow);
						var categoryCell = $('<td class="category-name">' + v['Name'] + '</td>').appendTo(newRow);
						var editCell = $('<td><a class="edit-category">Edit</a></td>').appendTo(newRow);
						var deleteCell = $('<td><a class="delete-category">Delete</a></td>').appendTo(newRow);
						var applyCell = $('<td><a class="apply-category">Apply</a></td>').appendTo(newRow);

					});
				} else if (info == 'Instructions') {
					$('<th>Id</th>').appendTo(headerRow);
					$('<th>Link</th>').appendTo(headerRow);
					$('<th>Header</th>').appendTo(headerRow);
					$('<th>Brand</th>').appendTo(headerRow);
					$('<th>Model</th>').appendTo(headerRow);
					$('<th>Category</th>').appendTo(headerRow);
					$('<th>Edit</th>').appendTo(headerRow);
					$('<th>Delete</th>').appendTo(headerRow);
					$('<th>Apply</th>').appendTo(headerRow);
					// console.log(data['AdminInfo'][info]);
					$.each(data['AdminInfo'][info], function (k, v) {
						// console.log(v);
						var newRow = $('<tr id="' + v['Id'] + '"></tr>').appendTo(infoTable);
						var idCell = $('<td>' + v['Id'] + '</td>').appendTo(newRow);
						var linkCell = $('<td class="instruction-link">' + v['Link'] + '</td>').appendTo(newRow);
						var headerCell = $('<td class="instruction-header">' + v['Header'] + '</td>').appendTo(newRow);
						var brandCell = $('<td class="instruction-brand">' + getObjects(data['AdminInfo']['Brands'], 'Id', v['Brand'])[0]['Brand'] + '</td>').appendTo(newRow);
						var modelCell = $('<td class="instruction-model">' + v['Model'] + '</td>').appendTo(newRow);
						var categoryCell = $('<td class="instruction-category">' + getObjects(data['AdminInfo']['Categories'], 'Id', v['Category'])[0]['Name'] + '</td>').appendTo(newRow);
						var editCell = $('<td><a class="edit-instruction">Edit</a></td>').appendTo(newRow);
						var deleteCell = $('<td><a class="delete-instruction">Delete</a></td>').appendTo(newRow);
						var applyCell = $('<td><a class="apply-instruction">Apply</a></td>').appendTo(newRow);

					});
				} else if (info == 'Users') {
					$('<th>Id</th>').appendTo(headerRow);
					$('<th>Email</th>').appendTo(headerRow);
					$('<th>Username</th>').appendTo(headerRow);
					$('<th>Auth Token</th>').appendTo(headerRow);
					$('<th>Is Admin</th>').appendTo(headerRow);
					$('<th>Edit</th>').appendTo(headerRow);
					$('<th>Delete</th>').appendTo(headerRow);
					$('<th>Apply</th>').appendTo(headerRow);
					// console.log(data['AdminInfo'][info]);
					$.each(data['AdminInfo'][info], function (k, v) {
						// console.log(v);
						var newRow = $('<tr id="' + v['Id'] + '"></tr>').appendTo(infoTable);
						var idCell = $('<td>' + v['Id'] + '</td>').appendTo(newRow);
						var emailCell = $('<td class="user-email">' + v['Email'] + '</td>').appendTo(newRow);
						var usernameCell = $('<td class="user-username">' + v['Username'] + '</td>').appendTo(newRow);
						var tokenCell = $('<td class="user-token">' + v['Auth_token'] + '</td>').appendTo(newRow);
						var isadminCell = $('<td class="user-isadmin">' + v['IsAdmin'] + '</td>').appendTo(newRow);
						var editCell = $('<td><a class="edit-user">Edit</a></td>').appendTo(newRow);
						var deleteCell = $('<td><a class="delete-user">Delete</a></td>').appendTo(newRow);
						var applyCell = $('<td><a class="apply-user">Apply</a></td>').appendTo(newRow);

					});
				} else if (info == 'Chart') {
					$('<th>Id</th>').appendTo(headerRow);
					$('<th>Singer</th>').appendTo(headerRow);
					$('<th>Song</th>').appendTo(headerRow);
					$('<th>Likes</th>').appendTo(headerRow);
					$('<th>Edit</th>').appendTo(headerRow);
					$('<th>Delete</th>').appendTo(headerRow);
					$('<th>Apply</th>').appendTo(headerRow);
					// console.log(data['AdminInfo'][info]);
					$.each(data['AdminInfo'][info], function (k, v) {
						// console.log(v);
						var newRow = $('<tr id="' + v['Id'] + '"></tr>').appendTo(infoTable);
						var idCell = $('<td>' + v['Id'] + '</td>').appendTo(newRow);
						var singerCell = $('<td class="chart-singer">' + v['Singer'] + '</td>').appendTo(newRow);
						var songCell = $('<td class="chart-song">' + v['Song'] + '</td>').appendTo(newRow);
						var likesCell = $('<td class="chart-likes">' + v['Likes'] + '</td>').appendTo(newRow);
						var editCell = $('<td><a class="edit-chart">Edit</a></td>').appendTo(newRow);
						var deleteCell = $('<td><a class="delete-chart">Delete</a></td>').appendTo(newRow);
						var applyCell = $('<td><a class="apply-chart">Apply</a></td>').appendTo(newRow);

					});
				}
			});

			$('.bot-page').click(function () {
				var botName = $(this).attr('bot');
				$.post("bot.php", {
					loadBot: botName,
				}).done(function(data){
					alert(data);
					location.reload();
				});
			});

			$('body').on('click', '.edit-row', function() {
				var parent = $(this).parent().parent();
				parent.find('.command').prop('disabled', function(i, v) { return !v; });
				parent.find('.reply').prop('disabled', function(i, v) { return !v; });
			});

			$('.add-row').click(function() {
				var table = $('.info-table');
				var id = parseInt($('tr').last().attr('id')) + 1;
				var info = getUrlParameter('info');
				if (info == 'Brands') {
					var newRow = $('<tr id="' + id + '"></tr>').appendTo(table);
					var idCell = $('<td>' + id + '</td>').appendTo(newRow);
					var brandCell = $('<input type="text" class="form-control brand-name" value="">').appendTo(newRow);
					var editCell = $('<td><a class="edit-brand">Edit</a></td>').appendTo(newRow);
					var deleteCell = $('<td><a class="delete-brand">Delete</a></td>').appendTo(newRow);
					var applyCell = $('<td><a class="apply-brand">Apply</a></td>').appendTo(newRow);
				} else if (info == 'Models') {
					var newRow = $('<tr id="' + id + '"></tr>').appendTo(table);
					var idCell = $('<td>' + id + '</td>').appendTo(newRow);
					var brandCell = $('<td><input type="text" class="form-control model-brand" value=""></td>').appendTo(newRow);
					var modelCell = $('<td><input type="text" class="form-control model-name" value=""></td>').appendTo(newRow);
					var editCell = $('<td><a class="edit-model">Edit</a></td>').appendTo(newRow);
					var deleteCell = $('<td><a class="delete-model">Delete</a></td>').appendTo(newRow);
					var applyCell = $('<td><a class="apply-model">Apply</a></td>').appendTo(newRow);					
				} else if (info == 'Categories') {
					var newRow = $('<tr id="' + id + '"></tr>').appendTo(table);
					var idCell = $('<td>' + id + '</td>').appendTo(newRow);
					var categoryCell = $('<input type="text" class="form-control category-name" value="">').appendTo(newRow);
					var editCell = $('<td><a class="edit-category">Edit</a></td>').appendTo(newRow);
					var deleteCell = $('<td><a class="delete-category">Delete</a></td>').appendTo(newRow);
					var applyCell = $('<td><a class="apply-category">Apply</a></td>').appendTo(newRow);				
				} else if (info == 'Instructions') {
					var newRow = $('<tr id="' + id + '"></tr>').appendTo(table);
					var idCell = $('<td>' + id + '</td>').appendTo(newRow);
					var linkCell = $('<td><input type="text" class="form-control instruction-link" value=""></td>').appendTo(newRow);
					var headerCell = $('<td><input type="text" class="form-control instruction-header" value=""></td></td>').appendTo(newRow);
					
					var brands = currentUserData['AdminInfo']['Brands'];
					var brandSelectTd = $('<td></td>').appendTo(newRow);
					var brandSelect = $('<select class="form-control instruction-brand" />').appendTo(brandSelectTd);
					$.each(brands, function (k, v) {
						if (k == '0') {
							var option = $('<option selected>' + v['Brand'] + '</option>').appendTo(brandSelect);
						} else {
							var option = $('<option>' + v['Brand'] + '</option>').appendTo(brandSelect);
						}
					});
					// var brandCell = $('<td><input type="text" class="form-control instruction-brand" value=""></td>').appendTo(newRow);
					var models = getObjects(currentUserData['AdminInfo']['Models'], "Brand", brands[0]['Id']);
					var modelsSelectTd = $('<td></td>').appendTo(newRow);
					var modelsSelect = $('<select class="form-control instruction-model" />').appendTo(modelsSelectTd);
					var option = $('<option selected></option>').appendTo(modelsSelect);
					$.each(models, function (k, v) {
						var option = $('<option>' + v['Model'] + '</option>').appendTo(modelsSelect);
					});
					// var modelCell = $('<td><input type="text" class="form-control instruction-model" value=""></td>').appendTo(newRow);

					var categoryCell = $('<td><input type="text" class="form-control instruction-category" value=""></td>').appendTo(newRow);
					var editCell = $('<td><a class="edit-instruction">Edit</a></td>').appendTo(newRow);
					var deleteCell = $('<td><a class="delete-instruction">Delete</a></td>').appendTo(newRow);
					var applyCell = $('<td><a class="apply-instruction">Apply</a></td>').appendTo(newRow);				
				} else if (info == 'Users') {
					var newRow = $('<tr id="' + id + '"></tr>').appendTo(table);
					var idCell = $('<td>' + id + '</td>').appendTo(newRow);
					var emailCell = $('<td><input type="text" class="form-control user-email" value=""></td>').appendTo(newRow);
					var usernameCell = $('<td><input type="text" class="form-control user-username" value=""></td>').appendTo(newRow);
					var tokenCell = $('<td><input type="text" class="form-control user-token" value="<?php echo strval(md5(crypto_rand(0, 262144))); ?>" disabled></td>').appendTo(newRow);
					var isadminCell = $('<td><select class="form-control user-isadmin"><option>True</option><option selected>False</option></select></td>').appendTo(newRow);
					var editCell = $('<td><a class="edit-user">Edit</a></td>').appendTo(newRow);
					var deleteCell = $('<td><a class="delete-user">Delete</a></td>').appendTo(newRow);
					var applyCell = $('<td><a class="apply-user">Apply</a></td>').appendTo(newRow);				
				} else if (info == 'Chart') {
					var newRow = $('<tr id="' + id + '"></tr>').appendTo(table);
					var idCell = $('<td>' + id + '</td>').appendTo(newRow);
					var singerCell = $('<td><input type="text" class="form-control chart-singer" value=""></td>').appendTo(newRow);
					var songCell = $('<td><input type="text" class="form-control chart-song" value=""></td>').appendTo(newRow);
					var likesCell = $('<td class="chart-likes">0</td>').appendTo(newRow);
					var editCell = $('<td><a class="edit-chart">Edit</a></td>').appendTo(newRow);
					var deleteCell = $('<td><a class="delete-chart">Delete</a></td>').appendTo(newRow);
					var applyCell = $('<td><a class="apply-chart">Apply</a></td>').appendTo(newRow);
				}
			});

			// BRAND MANAGEMENT //
			$('body').on('click', '.edit-brand', function() {
				var parent = $(this).parent().parent();

				if (parent.find('.brand-name').is('input')) {
					var td = $('<td class="brand-name">' + parent.find('.brand-name').val() + '</td>');
					parent.find('.brand-name').replaceWith(td);
				} else {
					var input = $('<input type="text" class="form-control brand-name" value="' + parent.find('.brand-name').html() + '">');
					parent.find('.brand-name').replaceWith(input);
				}
			});

			$('body').on('click', '.delete-brand', function() {
				var id = $(this).parent().parent().attr('id');
				$.post("index.php", {
					brandAction : 'Delete',
					id : id,
				}).done(function(data){
					alert(data);
					location.reload();
				});
			});

			$('body').on('click', '.apply-brand', function() {
				var id = $(this).parent().parent().attr('id');
				var brandCell = $(this).parent().parent().find('.brand-name');
				if (brandCell.is('input')) {
					var brand = brandCell.val();
				} else {
					var brand = brandCell.html();
				}

				$.post("index.php", {
					updateBrand: 'Update',
					id: id,
					brand: brand,
				}).done(function(data){
					alert(data);
					location.reload();
				});
			});
			// END BRAND MANAGEMENT //

			// MODEL MANAGEMENT //
			$('body').on('change', '.model-brand', function () {
				if ($(this).is('select')) {
					var parent = $(this).parent().parent();
					var model = parent.find('.model-name').parent();
					var brandName = $(this).val();

					var selectTd = $('<td></td>');
					var select = $('<select class="form-control model-name" />').appendTo(selectTd);

					var brand = getObjects(currentUserData['AdminInfo']['Brands'], "Brand", brandName)[0]['Id'];
					var models = getObjects(currentUserData['AdminInfo']['Models'], "Brand", brand);
					// console.log(model);
					$.each(models, function (k, v) {
						if (v['Model'] == model.html()) {
							var option = $('<option selected>' + v['Model'] + '</option>').appendTo(select);
						} else {
							var option = $('<option>' + v['Model'] + '</option>').appendTo(select);
						}
					});
					model.replaceWith(selectTd);
				}
			})

			$('body').on('click', '.edit-model', function() {
				var parent = $(this).parent().parent();

				var brand = parent.find('.model-brand');
				var model = parent.find('.model-name');
				console.log(model);
				if (brand.is('select')) {
					var td = $('<td class="model-brand">' + brand.val() + '</td>');
					brand.parent().replaceWith(td);
				} else {
					var selectTd = $('<td></td>');
					var select = $('<select class="form-control model-brand" />').appendTo(selectTd);
					// console.log(currentUserData);
					$.each(currentUserData['AdminInfo']['Brands'], function (k, v) {
						if (v['Brand'] == brand.html()) {
							var option = $('<option selected>' + v['Brand'] + '</option>').appendTo(select);
						} else {
							var option = $('<option>' + v['Brand'] + '</option>').appendTo(select);
						}
					})
					brand.replaceWith(selectTd);
				}

				if (model.is('select')) {
					var td = $('<td class="model-name">' + model.val() + '</td>');
					model.parent().replaceWith(td);
				} else {
					var selectTd = $('<td></td>');
					var select = $('<select class="form-control model-name" />').appendTo(selectTd);
					// console.log(parent.find('.model-brand').val());
					var brand = getObjects(currentUserData['AdminInfo']['Brands'], "Brand", parent.find('.model-brand').val())[0]['Id'];
					var models = getObjects(currentUserData['AdminInfo']['Models'], "Brand", brand);
					// console.log(model);
					$.each(models, function (k, v) {
						if (v['Model'] == model.html()) {
							var option = $('<option selected>' + v['Model'] + '</option>').appendTo(select);
						} else {
							var option = $('<option>' + v['Model'] + '</option>').appendTo(select);
						}
					});
					model.replaceWith(selectTd);
				}
			});

			$('body').on('click', '.delete-model', function() {
				var id = $(this).parent().parent().attr('id');
				$.post("index.php", {
					modelAction : 'Delete',
					id : id,
				}).done(function(data){
					alert(data);
					location.reload();
				});
			});

			$('body').on('click', '.apply-model', function() {
				var id = $(this).parent().parent().attr('id');
				var brandCell = $(this).parent().parent().find('.model-brand');
				var modelCell = $(this).parent().parent().find('.model-name');
				var brand = "";
				var model = "";
				if (brandCell.is('select') && modelCell.is('select')) {
					brand = brandCell.val();
					model = modelCell.val();
				} else if (brandCell.is('input') && modelCell.is('input')) {
					brand = brandCell.val();
					model = modelCell.val();
				} else {
					brand = brandCell.html();
					model = modelCell.html();
				}

				// alert(brand);
				// alert(model);

				$.post("index.php", {
					updateModel: 'Update',
					id: id,
					brand: brand,
					model: model,
				}).done(function(data){
					alert(data);
					location.reload();
				});
			});
			// END MODEL MANAGEMENT //

			// CATEGORY MANAGEMENT //
			$('body').on('click', '.edit-category', function() {
				var parent = $(this).parent().parent();

				if (parent.find('.category-name').is('input')) {
					var td = $('<td class="category-name">' + parent.find('.category-name').val() + '</td>');
					parent.find('.category-name').replaceWith(td);
				} else {
					var input = $('<input type="text" class="form-control category-name" value="' + parent.find('.category-name').html() + '">');
					parent.find('.category-name').replaceWith(input);
				}
			});

			$('body').on('click', '.delete-category', function() {
				var id = $(this).parent().parent().attr('id');
				$.post("index.php", {
					categoryAction : 'Delete',
					id : id,
				}).done(function(data){
					alert(data);
					location.reload();
				});
			});

			$('body').on('click', '.apply-category', function() {
				var id = $(this).parent().parent().attr('id');
				var categoryCell = $(this).parent().parent().find('.category-name');
				if (categoryCell.is('input')) {
					var category = categoryCell.val();
				} else {
					var category = categoryCell.html();
				}

				$.post("index.php", {
					updateCategory: 'Update',
					id: id,
					category: category,
				}).done(function(data){
					alert(data);
					location.reload();
				});
			});
			// END CATEGORY MANAGEMENT //

			// MODEL INSTRUCTIONS //
			$('body').on('change', '.instruction-brand', function () {
				if ($(this).is('select')) {
					var parent = $(this).parent().parent();
					var model = parent.find('.instruction-model').parent();
					var brandName = $(this).val();

					var selectTd = $('<td></td>');
					var select = $('<select class="form-control instruction-model" />').appendTo(selectTd);

					var brand = getObjects(currentUserData['AdminInfo']['Brands'], "Brand", brandName)[0]['Id'];
					var models = getObjects(currentUserData['AdminInfo']['Models'], "Brand", brand);
					// console.log(model);
					$.each(models, function (k, v) {
						if (v['Model'] == model.html()) {
							var option = $('<option selected>' + v['Model'] + '</option>').appendTo(select);
						} else {
							var option = $('<option>' + v['Model'] + '</option>').appendTo(select);
						}
					});
					model.replaceWith(selectTd);
				}
			});

			$('body').on('click', '.edit-instruction', function() {
				var parent = $(this).parent().parent();

				var link = parent.find('.instruction-link');
				var header = parent.find('.instruction-header');
				var brand = parent.find('.instruction-brand');
				var model = parent.find('.instruction-model');
				var category = parent.find('.instruction-category');
				// console.log(model);

				if (link.is('input')) {
					var td = $('<td class="instruction-link">' + link.val() + '</td>');
					link.parent().replaceWith(td);
				} else {
					var input = $('<td><input type="text" class="form-control instruction-link" value="' + link.html() + '"></td>');
					link.replaceWith(input);
				}

				if (header.is('input')) {
					var td = $('<td class="instruction-header">' + header.val() + '</td>');
					header.parent().replaceWith(td);
				} else {
					var input = $('<td><input type="text" class="form-control instruction-header" value="' + header.html() + '"></td>');
					header.replaceWith(input);
				}

				if (brand.is('select')) {
					var td = $('<td class="instruction-brand">' + brand.val() + '</td>');
					brand.parent().replaceWith(td);
				} else {
					var selectTd = $('<td></td>');
					var select = $('<select class="form-control instruction-brand" />').appendTo(selectTd);
					// console.log(currentUserData);
					$.each(currentUserData['AdminInfo']['Brands'], function (k, v) {
						if (v['Brand'] == brand.html()) {
							var option = $('<option selected>' + v['Brand'] + '</option>').appendTo(select);
						} else {
							var option = $('<option>' + v['Brand'] + '</option>').appendTo(select);
						}
					})
					brand.replaceWith(selectTd);
				}

				// console.log(model);
				if (model.is('select')) {
					var td = $('<td class="instruction-model">' + model.val() + '</td>');
					model.parent().replaceWith(td);
				} else {
					var selectTd = $('<td></td>');
					var select = $('<select class="form-control instruction-model" />').appendTo(selectTd);
					// console.log(parent.find('.model-brand').val());
					var brand = getObjects(currentUserData['AdminInfo']['Brands'], "Brand", parent.find('.instruction-brand').val())[0]['Id'];
					var models = getObjects(currentUserData['AdminInfo']['Models'], "Brand", brand);
					// console.log(model);
					$.each(models, function (k, v) {
						if (v['Model'] == model.html()) {
							var option = $('<option selected>' + v['Model'] + '</option>').appendTo(select);
						} else {
							var option = $('<option>' + v['Model'] + '</option>').appendTo(select);
						}
					});
					model.replaceWith(selectTd);
				}

				if (category.is('select')) {
					var td = $('<td class="instruction-category">' + category.val() + '</td>');
					category.parent().replaceWith(td);
				} else {
					var selectTd = $('<td></td>');
					var select = $('<select class="form-control instruction-category" />').appendTo(selectTd);
					// console.log(currentUserData);
					$.each(currentUserData['AdminInfo']['Categories'], function (k, v) {
						if (v['Name'] == category.html()) {
							var option = $('<option selected>' + v['Name'] + '</option>').appendTo(select);
						} else {
							var option = $('<option>' + v['Name'] + '</option>').appendTo(select);
						}
					})
					category.replaceWith(selectTd);
				}
			});

			$('body').on('click', '.delete-instruction', function() {
				var id = $(this).parent().parent().attr('id');
				$.post("index.php", {
					instructionAction : 'Delete',
					id : id,
				}).done(function(data){
					alert(data);
					location.reload();
				});
			});

			$('body').on('click', '.apply-instruction', function() {
				var id = $(this).parent().parent().attr('id');
				var linkCell = $(this).parent().parent().find('.instruction-link');
				var headerCell = $(this).parent().parent().find('.instruction-header');
				var brandCell = $(this).parent().parent().find('.instruction-brand');
				var modelCell = $(this).parent().parent().find('.instruction-model');
				var categoryCell = $(this).parent().parent().find('.instruction-category');
				var link = "";
				var header = "";
				var brand = "";
				var model = "";
				var category = "";
				if (linkCell.is('input') && headerCell.is('input')) {
					link = linkCell.val();
					header = headerCell.val();
				} else {
					link = linkCell.html();
					header = headerCell.html();					
				}

				if (brandCell.is('select') && modelCell.is('select')) {
					brand = brandCell.val();
					model = modelCell.val();
				} else if (brandCell.is('input') && modelCell.is('input')) {
					brand = brandCell.val();
					model = modelCell.val();
				} else {
					brand = brandCell.html();
					model = modelCell.html();
				}

				if (categoryCell.is('select') || categoryCell.is('input')) {
					category = categoryCell.val();
				} else {
					category = categoryCell.html();
				}

				// alert(brand);
				// alert(model);
				console.log(brand);
				console.log(model);
				console.log(category);

				$.post("index.php", {
					updateInstruction: 'Update',
					id: id,
					link: link,
					header: header,
					brandName: brand,
					modelName: model,
					categoryName: category,
				}).done(function(data){
					alert(data);
					location.reload();
				});
			});
			// END INSTRUCTIONS MANAGEMENT //

			// USERS MANAGEMENT //
			$('body').on('click', '.edit-user', function() {
				var parent = $(this).parent().parent();
				var isAdmin = parent.find('.user-isadmin');

				if (isAdmin.is('select')) {
					var td = $('<td class="user-isadmin">' + isAdmin.val() + '</td>');
					isAdmin.parent().replaceWith(td);
				} else {
					if (isAdmin.html() == 'True') { var select = $('<td><select class="form-control user-isadmin"><option selected>True</option><option>False</option></select></td>'); }
					else if (isAdmin.html() == 'False') { var select = $('<td><select class="form-control user-isadmin"><option>True</option><option selected>False</option></select></td>'); }
					isAdmin.replaceWith(select);
				}
			});

			$('body').on('click', '.delete-user', function() {
				var id = $(this).parent().parent().attr('id');
				$.post("index.php", {
					userAction : 'Delete',
					id : id,
				}).done(function(data){
					alert(data);
					location.reload();
				});
			});

			$('body').on('click', '.apply-user', function() {
				var id = $(this).parent().parent().attr('id');
				var emailCell = $(this).parent().parent().find('.user-email');
				var usernameCell = $(this).parent().parent().find('.user-username');
				var tokenCell = $(this).parent().parent().find('.user-token');
				var isAdminCell = $(this).parent().parent().find('.user-isadmin');

				if (emailCell.is('input') && usernameCell.is('input') && tokenCell.is('input')) {
					var email = emailCell.val();
					var username = usernameCell.val();
					var token = tokenCell.val();
				} else {
					var email = emailCell.html();
					var username = usernameCell.html();
					var token = tokenCell.html();
				}

				if (isAdminCell.is('select')) {
					var isAdmin = isAdminCell.val();
				} else {
					var isAdmin = isAdminCell.html();
				}

				$.post("index.php", {
					updateUser: 'Update',
					id: id,
					email: email,
					username: username,
					token: token,
					isAdmin: isAdmin,
				}).done(function(data){
					alert(data);
					location.reload();
				});
			});
			// END USERS MANAGEMENT //

			// CHART MANAGEMENT //
			$('body').on('click', '.edit-chart', function() {
				var parent = $(this).parent().parent();
				var singer = parent.find('.chart-singer');
				var song = parent.find('.chart-song');

				if (singer.is('input') && song.is('input')) {
					var singerTd = $('<td class="chart-singer">' + singer.val() + '</td>');
					var songTd = $('<td class="chart-song">' + song.val() + '</td>');
					singer.parent().replaceWith(singerTd);
					song.parent().replaceWith(songTd);
				} else {
					var singerInput = $('<td><input class="form-control chart-singer" value="' + singer.html() + '"></td>');
					var songInput = $('<td><input class="form-control chart-song" value="' + song.html() + '"></td>');
					singer.replaceWith(singerInput);
					song.replaceWith(songInput);
				}
			});

			$('body').on('click', '.delete-chart', function() {
				var id = $(this).parent().parent().attr('id');
				$.post("index.php", {
					chartAction : 'Delete',
					id : id,
				}).done(function(data){
					alert(data);
					location.reload();
				});
			});

			$('body').on('click', '.apply-chart', function() {
				var id = $(this).parent().parent().attr('id');
				var singerCell = $(this).parent().parent().find('.chart-singer');
				var songCell = $(this).parent().parent().find('.chart-song');
				var likesCell = $(this).parent().parent().find('.chart-likes');

				var likes = likesCell.html();

				if (singerCell.is('input') && songCell.is('input')) {
					var singer = singerCell.val();
					var song = songCell.val();
				} else {
					var singer = singerCell.html();
					var song = songCell.html();
				}

				$.post("index.php", {
					updateChart: 'Update',
					id: id,
					singer: singer,
					song: song,
					likes: likes,
				}).done(function(data){
					alert(data);
					location.reload();
				});
			});
			// END CHART MANAGEMENT //

			$('#logout').click(function(){
				$.post("index.php", {
					Logout: 'Logout',
				}).done(function(data) {
					location = data;
				});
			});
		});
	</script>
</head>

<body>

<section id="container" >
<!--header start-->
<header class="header fixed-top clearfix">
<!--logo start-->
<div class="brand">

	<a href="index.php" class="logo">
		<img src="template/images/logo_e2.png" alt="">
	</a>
	<div class="sidebar-toggle-box">
		<div class="fa fa-bars"></div>
	</div>
</div>
<!--logo end-->

<div class="nav notify-row" id="top_menu">
	<!--  notification start -->
	<ul class="nav top-menu">
		<!-- settings start -->
		<li class="dropdown">
			<a data-toggle="dropdown" class="dropdown-toggle" href="#">
				<i class="fa fa-tasks"></i>
				<span class="badge bg-success">8</span>
			</a>
			<ul class="dropdown-menu extended tasks-bar">
				<li>
					<p class="">You have 8 pending tasks</p>
				</li>
				<li>
					<a href="#">
						<div class="task-info clearfix">
							<div class="desc pull-left">
								<h5>Target Sell</h5>
								<p>25% , Deadline  12 June’13</p>
							</div>
									<span class="notification-pie-chart pull-right" data-percent="45">
							<span class="percent"></span>
							</span>
						</div>
					</a>
				</li>
				<li>
					<a href="#">
						<div class="task-info clearfix">
							<div class="desc pull-left">
								<h5>Product Delivery</h5>
								<p>45% , Deadline  12 June’13</p>
							</div>
									<span class="notification-pie-chart pull-right" data-percent="78">
							<span class="percent"></span>
							</span>
						</div>
					</a>
				</li>
				<li>
					<a href="#">
						<div class="task-info clearfix">
							<div class="desc pull-left">
								<h5>Payment collection</h5>
								<p>87% , Deadline  12 June’13</p>
							</div>
									<span class="notification-pie-chart pull-right" data-percent="60">
							<span class="percent"></span>
							</span>
						</div>
					</a>
				</li>
				<li>
					<a href="#">
						<div class="task-info clearfix">
							<div class="desc pull-left">
								<h5>Target Sell</h5>
								<p>33% , Deadline  12 June’13</p>
							</div>
									<span class="notification-pie-chart pull-right" data-percent="90">
							<span class="percent"></span>
							</span>
						</div>
					</a>
				</li>

				<li class="external">
					<a href="#">See All Tasks</a>
				</li>
			</ul>
		</li>
		<!-- settings end -->
		<!-- inbox dropdown start-->
		<li id="header_inbox_bar" class="dropdown">
			<a data-toggle="dropdown" class="dropdown-toggle" href="#">
				<i class="fa fa-envelope-o"></i>
				<span class="badge bg-important">4</span>
			</a>
			<ul class="dropdown-menu extended inbox">
				<li>
					<p class="red">You have 4 Mails</p>
				</li>
				<li>
					<a href="#">
						<span class="photo"><img alt="avatar" src="images/avatar-mini.jpg"></span>
								<span class="subject">
								<span class="from">Jonathan Smith</span>
								<span class="time">Just now</span>
								</span>
								<span class="message">
									Hello, this is an example msg.
								</span>
					</a>
				</li>
				<li>
					<a href="#">
						<span class="photo"><img alt="avatar" src="images/avatar-mini-2.jpg"></span>
								<span class="subject">
								<span class="from">Jane Doe</span>
								<span class="time">2 min ago</span>
								</span>
								<span class="message">
									Nice admin template
								</span>
					</a>
				</li>
				<li>
					<a href="#">
						<span class="photo"><img alt="avatar" src="images/avatar-mini-3.jpg"></span>
								<span class="subject">
								<span class="from">Tasi sam</span>
								<span class="time">2 days ago</span>
								</span>
								<span class="message">
									This is an example msg.
								</span>
					</a>
				</li>
				<li>
					<a href="#">
						<span class="photo"><img alt="avatar" src="images/avatar-mini.jpg"></span>
								<span class="subject">
								<span class="from">Mr. Perfect</span>
								<span class="time">2 hour ago</span>
								</span>
								<span class="message">
									Hi there, its a test
								</span>
					</a>
				</li>
				<li>
					<a href="#">See all messages</a>
				</li>
			</ul>
		</li>
		<!-- inbox dropdown end -->
		<!-- notification dropdown start-->
		<li id="header_notification_bar" class="dropdown">
			<a data-toggle="dropdown" class="dropdown-toggle" href="#">

				<i class="fa fa-bell-o"></i>
				<span class="badge bg-warning">3</span>
			</a>
			<ul class="dropdown-menu extended notification">
				<li>
					<p>Notifications</p>
				</li>
				<li>
					<div class="alert alert-info clearfix">
						<span class="alert-icon"><i class="fa fa-bolt"></i></span>
						<div class="noti-info">
							<a href="#"> Server #1 overloaded.</a>
						</div>
					</div>
				</li>
				<li>
					<div class="alert alert-danger clearfix">
						<span class="alert-icon"><i class="fa fa-bolt"></i></span>
						<div class="noti-info">
							<a href="#"> Server #2 overloaded.</a>
						</div>
					</div>
				</li>
				<li>
					<div class="alert alert-success clearfix">
						<span class="alert-icon"><i class="fa fa-bolt"></i></span>
						<div class="noti-info">
							<a href="#"> Server #3 overloaded.</a>
						</div>
					</div>
				</li>

			</ul>
		</li>
		<!-- notification dropdown end -->
	</ul>
	<!--  notification end -->
</div>
<div class="top-nav clearfix">
	<!--search & user info start-->
	<ul class="nav pull-right top-menu">
		<li>
			<input type="text" class="form-control search" placeholder=" Search">
		</li>
		<!-- user login dropdown start-->
		<li class="dropdown">
			<a data-toggle="dropdown" class="dropdown-toggle" href="#">
				<img alt="" src="template/images/avatar1_small.jpg">
				<span class="username">John Doe</span>
				<b class="caret"></b>
			</a>
			<ul class="dropdown-menu extended logout">
				<li><a href="#"><i class=" fa fa-suitcase"></i>Profile</a></li>
				<li><a href="#"><i class="fa fa-cog"></i> Settings</a></li>
				<li><a href="#" id="logout"><i class="fa fa-key"></i> Log Out</a></li>
			</ul>
		</li>
		<!-- user login dropdown end -->
		<li>
			<div class="toggle-right-box">
				<div class="fa fa-bars"></div>
			</div>
		</li>
	</ul>
	<!--search & user info end-->
</div>
</header>
<!--header end-->
<aside>
	<div id="sidebar" class="nav-collapse">
		<!-- sidebar menu start-->  <div class="leftside-navigation">
			<ul class="sidebar-menu" id="nav-accordion">
				<li>
					<a class="active" href="index.php">
						<i class="fa fa-dashboard"></i>
						<span>Create new bot</span>
					</a>
				</li>
				<li class="sub-menu">
                    <a data-toggle="collapse" href=".bots-submenu">
                        <i class="fa fa-laptop"></i>
                        <span>My bots</span>
                    </a>
                    <ul class="sub bots-submenu collapse">
                    </ul>
                </li>
		</ul></div>  
<!-- sidebar menu end-->
	</div>
</aside>
<!--sidebar end-->
	<!--main content start-->
	<section id="main-content">
		<section class="wrapper">
		<!-- page start-->

		<div class="row">
			<div class="col-sm-12">
				<section class="panel">
					<header class="panel-heading">
						Editable Table
						<span class="tools pull-right">
							<a href="javascript:;" class="fa fa-chevron-down"></a>
							<a href="javascript:;" class="fa fa-cog"></a>
							<a href="javascript:;" class="fa fa-times"></a>
						 </span>
					</header>
					<div class="panel-body">
						<div class="adv-table editable-table ">
							<div class="clearfix">
								<div class="btn-group">
									<button class="btn btn-primary add-row">
										Add New <i class="fa fa-plus"></i>
									</button>
								</div>
								<!-- <div class="btn-group pull-right">
									<button class="btn btn-default dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>
									</button>
									<ul class="dropdown-menu pull-right">
										<li><a href="#">Print</a></li>
										<li><a href="#">Save as PDF</a></li>
										<li><a href="#">Export to Excel</a></li>
									</ul>
								</div> -->
							</div>
							<div class="space15"></div>
							<table class="table table-striped table-hover table-bordered info-table" id="editable-sample">
								<thead>
								</thead>
								<tbody>
									<tr id="sample"></tr>
								</tbody>
							</table>
						</div>
					</div>
				</section>
			</div>
		</div>
		<!-- page end-->
		</section>
	</section>
	<!--main content end-->
<!--right sidebar start-->
<div class="right-sidebar">
<div class="search-row">
	<input type="text" placeholder="Search" class="form-control">
</div>
<div class="right-stat-bar">
<ul class="right-side-accordion">
<li class="widget-collapsible">
	<a href="#" class="head widget-head red-bg active clearfix">
		<span class="pull-left">work progress (5)</span>
		<span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
	</a>
	<ul class="widget-container">
		<li>
			<div class="prog-row side-mini-stat clearfix">
				<div class="side-graph-info">
					<h4>Target sell</h4>
					<p>
						25%, Deadline 12 june 13
					</p>
				</div>
				<div class="side-mini-graph">
					<div class="target-sell">
					</div>
				</div>
			</div>
			<div class="prog-row side-mini-stat">
				<div class="side-graph-info">
					<h4>product delivery</h4>
					<p>
						55%, Deadline 12 june 13
					</p>
				</div>
				<div class="side-mini-graph">
					<div class="p-delivery">
						<div class="sparkline" data-type="bar" data-resize="true" data-height="30" data-width="90%" data-bar-color="#39b7ab" data-bar-width="5" data-data="[200,135,667,333,526,996,564,123,890,564,455]">
						</div>
					</div>
				</div>
			</div>
			<div class="prog-row side-mini-stat">
				<div class="side-graph-info payment-info">
					<h4>payment collection</h4>
					<p>
						25%, Deadline 12 june 13
					</p>
				</div>
				<div class="side-mini-graph">
					<div class="p-collection">
						<span class="pc-epie-chart" data-percent="45">
						<span class="percent"></span>
						</span>
					</div>
				</div>
			</div>
			<div class="prog-row side-mini-stat">
				<div class="side-graph-info">
					<h4>delivery pending</h4>
					<p>
						44%, Deadline 12 june 13
					</p>
				</div>
				<div class="side-mini-graph">
					<div class="d-pending">
					</div>
				</div>
			</div>
			<div class="prog-row side-mini-stat">
				<div class="col-md-12">
					<h4>total progress</h4>
					<p>
						50%, Deadline 12 june 13
					</p>
					<div class="progress progress-xs mtop10">
						<div style="width: 50%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="20" role="progressbar" class="progress-bar progress-bar-info">
							<span class="sr-only">50% Complete</span>
						</div>
					</div>
				</div>
			</div>
		</li>
	</ul>
</li>
<li class="widget-collapsible">
	<a href="#" class="head widget-head terques-bg active clearfix">
		<span class="pull-left">contact online (5)</span>
		<span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
	</a>
	<ul class="widget-container">
		<li>
			<div class="prog-row">
				<div class="user-thumb">
					<a href="#"><img src="template/images/avatar1_small.jpg" alt=""></a>
				</div>
				<div class="user-details">
					<h4><a href="#">Jonathan Smith</a></h4>
					<p>
						Work for fun
					</p>
				</div>
				<div class="user-status text-danger">
					<i class="fa fa-comments-o"></i>
				</div>
			</div>
			<div class="prog-row">
				<div class="user-thumb">
					<a href="#"><img src="template/images/avatar1.jpg" alt=""></a>
				</div>
				<div class="user-details">
					<h4><a href="#">Anjelina Joe</a></h4>
					<p>
						Available
					</p>
				</div>
				<div class="user-status text-success">
					<i class="fa fa-comments-o"></i>
				</div>
			</div>
			<div class="prog-row">
				<div class="user-thumb">
					<a href="#"><img src="template/images/chat-avatar2.jpg" alt=""></a>
				</div>
				<div class="user-details">
					<h4><a href="#">John Doe</a></h4>
					<p>
						Away from Desk
					</p>
				</div>
				<div class="user-status text-warning">
					<i class="fa fa-comments-o"></i>
				</div>
			</div>
			<div class="prog-row">
				<div class="user-thumb">
					<a href="#"><img src="template/images/avatar1_small.jpg" alt=""></a>
				</div>
				<div class="user-details">
					<h4><a href="#">Mark Henry</a></h4>
					<p>
						working
					</p>
				</div>
				<div class="user-status text-info">
					<i class="fa fa-comments-o"></i>
				</div>
			</div>
			<div class="prog-row">
				<div class="user-thumb">
					<a href="#"><img src="template/images/avatar1.jpg" alt=""></a>
				</div>
				<div class="user-details">
					<h4><a href="#">Shila Jones</a></h4>
					<p>
						Work for fun
					</p>
				</div>
				<div class="user-status text-danger">
					<i class="fa fa-comments-o"></i>
				</div>
			</div>
			<p class="text-center">
				<a href="#" class="view-btn">View all Contacts</a>
			</p>
		</li>
	</ul>
</li>
<li class="widget-collapsible">
	<a href="#" class="head widget-head purple-bg active">
		<span class="pull-left"> recent activity (3)</span>
		<span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
	</a>
	<ul class="widget-container">
		<li>
			<div class="prog-row">
				<div class="user-thumb rsn-activity">
					<i class="fa fa-clock-o"></i>
				</div>
				<div class="rsn-details ">
					<p class="text-muted">
						just now
					</p>
					<p>
						<a href="#">Jim Doe </a>Purchased new equipments for zonal office setup
					</p>
				</div>
			</div>
			<div class="prog-row">
				<div class="user-thumb rsn-activity">
					<i class="fa fa-clock-o"></i>
				</div>
				<div class="rsn-details ">
					<p class="text-muted">
						2 min ago
					</p>
					<p>
						<a href="#">Jane Doe </a>Purchased new equipments for zonal office setup
					</p>
				</div>
			</div>
			<div class="prog-row">
				<div class="user-thumb rsn-activity">
					<i class="fa fa-clock-o"></i>
				</div>
				<div class="rsn-details ">
					<p class="text-muted">
						1 day ago
					</p>
					<p>
						<a href="#">Jim Doe </a>Purchased new equipments for zonal office setup
					</p>
				</div>
			</div>
		</li>
	</ul>
</li>
<li class="widget-collapsible">
	<a href="#" class="head widget-head yellow-bg active">
		<span class="pull-left"> shipment status</span>
		<span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
	</a>
	<ul class="widget-container">
		<li>
			<div class="col-md-12">
				<div class="prog-row">
					<p>
						Full sleeve baby wear (SL: 17665)
					</p>
					<div class="progress progress-xs mtop10">
						<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
							<span class="sr-only">40% Complete</span>
						</div>
					</div>
				</div>
				<div class="prog-row">
					<p>
						Full sleeve baby wear (SL: 17665)
					</p>
					<div class="progress progress-xs mtop10">
						<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
							<span class="sr-only">70% Completed</span>
						</div>
					</div>
				</div>
			</div>
		</li>
	</ul>
</li>
</ul>
</div>
</div>
<!--right sidebar end-->

</section>

<!-- Placed js at the end of the document so the pages load faster -->

<!--Core js-->
<script src="template/js/jquery-1.10.2.min.js"></script>
<script src="template/js/jquery-migrate.js"></script>

<script src="template/bs3/js/bootstrap.min.js"></script>
<script class="include" type="text/javascript" src="template/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="template/js/jquery.scrollTo.min.js"></script>
<script src="template/js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script src="template/js/jquery.nicescroll.js"></script>
<!--Easy Pie Chart-->
<script src="template/js/easypiechart/jquery.easypiechart.js"></script>
<!--Sparkline Chart-->
<script src="template/js/sparkline/jquery.sparkline.js"></script>
<!--jQuery Flot Chart-->
<script src="template/js/flot-chart/jquery.flot.js"></script>
<script src="template/js/flot-chart/jquery.flot.tooltip.min.js"></script>
<script src="template/js/flot-chart/jquery.flot.resize.js"></script>
<script src="template/js/flot-chart/jquery.flot.pie.resize.js"></script>

<!-- <script type="text/javascript" src="template/js/data-tables/jquery.dataTables.js"></script> -->
<script type="text/javascript" src="template/js/data-tables/DT_bootstrap.js"></script>

<!--common script init for all pages-->
<script src="template/js/scripts.js"></script>

<!--script for this page only-->
<script src="template/js/table-editable.js"></script>

<!-- END JAVASCRIPTS -->
<script>
	jQuery(document).ready(function() {
		EditableTable.init();
	});
</script>

</body>
</html>
