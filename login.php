<?php
	include 'functions.php';
	session_start();
	if (isset($_POST['authorize'])) {
		$email = $_POST['inputEmail'];
		$password = $_POST['inputPassword'];
		$user = select('*', 'Users', 'Email=\'' . $email. '\' AND Password=\'' . $password . '\'')[0];
		if ($user != []) {
			$auth_token = $user['Auth_token'];
			if ($auth_token == null) {
				$auth_token = strval(md5(crypto_rand(0, 262144)));
				update('Users', 'Auth_token=\'' . $auth_token . '\'', 'Id=\'' . $user['Id'] . '\'');
			}
			if (isset($_POST['rememberValue']) and $_POST['rememberValue'] == 'remember-me') {
				setcookie("AUTH_TOKEN", $auth_token);				
			}
			$_SESSION['User'] = $user;
			// die(print_r($_SESSION));
			header('Location: /index.php');
			die();
		} else {
			header('Location: /login.php');
			die();
		}
	} else {
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="template/images/favicon.png">

    <title>Login</title>

    <!--Core CSS -->
    <link href="template/bs3/css/bootstrap.min.css" rel="stylesheet">
    <link href="template/css/bootstrap-reset.css" rel="stylesheet">
    <link href="template/font-awesome/css/font-awesome.css" rel="stylesheet" />

    <!-- Custom styles for this template -->
    <link href="template/css/style.css" rel="stylesheet">
    <link href="template/css/style-responsive.css" rel="stylesheet" />

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]>
    <script src="js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

  <body class="login-body">

    <div class="container">

      <form class="form-signin" action="login.php" method="post">
        <h2 class="form-signin-heading">sign in now</h2>
        <div class="login-wrap">
            <div class="user-login-info">
                <input name="authorize" hidden>
                <input name="inputEmail" type="text" class="form-control" placeholder="Email" autofocus>
                <input name="inputPassword" type="password" class="form-control" placeholder="Password">
            </div>
            <label class="checkbox">
                <input type="checkbox" value="remember-me" name="rememberValue"> Remember me
                <span class="pull-right">
                    <a data-toggle="modal" href="#myModal"> Forgot Password?</a>

                </span>
            </label>
            <button class="btn btn-lg btn-login btn-block" type="submit">Sign in</button>

            <div class="registration">
                Don't have an account yet?
                <a class="" href="registration.html">
                    Create an account
                </a>
            </div>

        </div>

          <!-- Modal -->
          <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
              <div class="modal-dialog">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                          <h4 class="modal-title">Forgot Password ?</h4>
                      </div>
                      <div class="modal-body">
                          <p>Enter your e-mail address below to reset your password.</p>
                          <input type="text" name="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">

                      </div>
                      <div class="modal-footer">
                          <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                          <button class="btn btn-success" type="button">Submit</button>
                      </div>
                  </div>
              </div>
          </div>
          <!-- modal -->

      </form>

    </div>



    <!-- Placed js at the end of the document so the pages load faster -->

    <!--Core js-->
    <script src="js/jquery.js"></script>
    <script src="bs3/js/bootstrap.min.js"></script>

  </body>
</html>