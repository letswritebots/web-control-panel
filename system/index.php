<?php
	header('Content-Type: text/html; charset=utf-8');
	header('Access-Control-Allow-Origin: *');
	mb_internal_encoding('UTF-8');
	setlocale(LC_ALL, 'ru_RU.UTF-8');
	ini_set('memory_limit', '-1');
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	include 'system/functions.php';
	// 188.120.233.65 
	// telegram 
	// telegram12345 
	// пользователь mysql

	class Command {
		var $db;
		var $id;
		var $text;
		var $reply;
		var $options;

		function __construct($db, $id, $text, $reply, $options) {
			$this->db = $db;
			$this->id = $id;
			$this->text = $text;
			$this->reply = $reply;
			$this->options = $options;
		}

		function applyCommand() {
			$db = $this->db;
			$id = $this->id;
			$text = mysql_escape_string($this->text);
			$reply = mysql_escape_string($this->reply);
			
			if ($text == '' || $reply == '') {
				delete($db, 'Commands', 'Id=' . $id);
				// delete($db, 'Commands', 'Id=\'' . $id . '\'');
			}

			$prevCommand = select($db, '*', 'Commands', 'Id=\'' . $id . '\'');
			$prevCommandText = $prevCommand[0]['Text'];

			if ($prevCommandText == []) {
				insert($db, 'Commands', 'Text, Reply', '\'' . $text . '\', \'' . $reply . '\'');
			} else {
				update($db, 'Commands', 'Text=\'' . $text . '\', Reply=\'' . $reply . '\'', 'Id=\'' . $id . '\'');
			}
		}
	}

	class Bot {
		var $db;
		var $id;
		var $username;
		var $name;
		var $commands;

		function __construct($db, $username, $name, $commands) {
			$this->db = $db;
			$this->username = $username;
			$this->name = $name;
			$this->commands = $commands;
		}
	}

	class Client {
		var $db;
		var $id;
		var $email;
		var $username;
		var $isAdmin;
		var $bots;

		function __construct($db, $email) {
			$this->db = $db;
			$this->email = $email;
			$this->id = select($db, 'Id', 'Users', 'Email=\'' . $email . '\'')[0]['Id'];
			$this->username = select($db, 'Username', 'Users', 'Email=\'' . $email . '\'')[0]['Username'];
			$this->isAdmin = select($db, 'IsAdmin', 'Users', 'Email=\'' . $email . '\'')[0]['IsAdmin'];
		}

		function createNewBot($botUsername, $botName) {
			$bot = new Bot($db, $botUsername, $botName, []);
			array_push($this->bots, $bot);

			$fields = [
				'Id' => 'int',
				'Text' => 'varchar (255)',
				'Reply' => 'varchar (255)'
			];

			insert($db, 'Bots', 'Bot_username, Bot_name', '\'' . $bot->username . '\', \'' . $bot->name . '\'');
			create($this->db, $this->username . '_' . $bot->username . '_Commands', $fields);
		}

		function getBotsFromDb() {
			$this->bots = select($db, '*', 'ClientsBots', 'ClientId=\'' . $this->id . '\'');
			foreach ($this->bots as $k => $v) {
				
			}
		}
	}

	session_start();

	$connectionData['HOST'] = '188.120.233.65';
	$connectionData['USER'] = 'telegram';
	$connectionData['PASSWORD'] = 'telegram12345';
	$connectionData['BASE'] = 'bot_constructor';

	// $connectionData['HOST'] = 'offerplus.ipagemysql.com';
	// $connectionData['USER'] = 'telegram';
	// $connectionData['PASSWORD'] = 'telegram12345';
	// $connectionData['BASE'] = 'bot_constructor';

	$db = new mysqli($connectionData['HOST'], $connectionData['USER'], $connectionData['PASSWORD'], $connectionData['BASE'], "3306");
	$db->set_charset("utf8");

	$_SESSION['connectionData'] = $connectionData;
	
	if (isset($_POST['Logout']) and $_POST['Logout'] != null) {
		unset($_COOKIE['AUTH_TOKEN']);
		$_SESSION['User'] = null;
	}

	if (isset($_SESSION['User']) and $_SESSION['User'] != null) {
		$user = $_SESSION['User'];
		if ($user['IsAdmin'] == 'True') {
			header('Location: /admin.php/');
			die();	
		}
	}

	if (isset($_COOKIE["AUTH_TOKEN"]) and $_COOKIE["AUTH_TOKEN"] != null) {
		$user = select($db, '*', 'Users', 'Auth_token=\'' . $_COOKIE['AUTH_TOKEN'] . '\'')[0];
		if ($user['IsAdmin'] == 'True') {
			header('Location: /admin.php/');
			die();	
		}
	}
	
	if (!isset($_SESSION['User']) and !isset($_COOKIE["AUTH_TOKEN"])) {
		header('Location: /login.html');
		die();
	}



	if (isset($_GET['Test'])) {
		$my_command = new Command($db, '3', 'Hello, user!', ['Hello, bot! How are you?11', 'What\'s day today?']);
		$my_command->applyCommand();
	}

	if (isset($_POST['updateBot']) and $_POST['updateBot'] == 'Update') {
		$commands = $_POST['commandArray'];
		//die(print_r($commands));
		foreach ($commands as $k => $v) {
			$commandId = $v['commandId'];
			$command = mysql_escape_string($v['command']);
			$reply = mysql_escape_string($v['reply']);
			$options = $v['options'];

			$newCommand = new Command($db, $commandId, $command, $reply, $options);
			$newCommand->applyCommand();
		}
		die('Success');
	}

	if (isset($_GET['getCommands']) and $_GET['getCommands'] == 'GET') {
		$commands = select($db, '*', 'Commands', NULL);
		$commandArray = [];

		foreach ($commands as $k => $v) {
			$commandArray[$k] = array();
			$commandArray[$k]['Id'] = $v['Id'];
			$commandArray[$k]['Text'] = str_replace('\\', '', $v['Text']);
			$commandArray[$k]['Reply'] = str_replace('\\', '', $v['Reply']);
		}
		$commandArray['Username'] = $_SESSION['User']['Username'];
		echo json_encode($commandArray); die();
	}

	if (isset($_POST['deleteCommand']) and ($_POST['deleteCommand'] == 'Delete')) {
		$commandId = $_POST['commandId'];
		delete($db, 'Commands', 'Id=\'' . $commandId . '\'');
		die('Success');
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Bot Constructor</title>
	<script src="/template/js/jquery.min.js" type="text/javascript"></script>
	<script src="/template/js/bootstrap.min.js" type="text/javascript"></script>
	<link href="template/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="template/css/button-style.css" rel="stylesheet" type="text/css">
	<style type="text/css">
		@media (min-width: 768px) {
			.navbar-collapse {
				height: auto;
				border-top: 0;
				box-shadow: none;
				max-height: none;
				padding-left:0;
				padding-right:0;
			}
			.navbar-collapse.collapse {
				display: block !important;
				width: auto !important;
				padding-bottom: 0;
				overflow: visible !important;
			}
			.navbar-collapse.in {
				overflow-x: visible;
			}

			.navbar
			{
				max-width:300px;
				margin-right: 0;
				margin-left: 0;
			}	

			.navbar-nav,
			.navbar-nav > li,
			.navbar-left,
			.navbar-right,
			.navbar-header
			{float:none !important;}

			.navbar-right .dropdown-menu {left:0;right:auto;}
			.navbar-collapse .navbar-nav.navbar-right:last-child {
				margin-right: 0;
			}
		}

		#apply_bot_token, #logoutIcon {
			cursor: pointer;			
		}
	</style>
	<script type="text/javascript">
		$(document).ready(function(){
			var loadCommands = $.getJSON("/?getCommands=GET")
					.done(function(data) {
						$('#header-username').text(data['Username']).append("&nbsp;&nbsp;");
						$.each(data, function (k, v) {
							if (typeof v == 'object') {
								var commandId = v['Id'];
								var commandText = v['Text'];
								var commandReply = v['Reply'];

								var table_row = $('<tr class="command-' + commandId + '"/>').appendTo($('#test-table'));
								var command_cell = $('<td>' + '<input type="text" class="command form-control" value="' + commandText + '">' + '</td>').appendTo(table_row);
								var reply_cell = $('<td>' + '<input type="text" class="reply form-control" value="' + commandReply + '">' + '</td>').appendTo(table_row);
								var delete_btn = $('<td><button class="btn lg ghost" name="delete_command" command="' + commandId + '">DEL</button></td>').appendTo(table_row);
							}
						});
					});

			$('button[name=submit]').click(function () {
				var commands = $("tr[class^='command']");
				var commandArray = [];
				$.each(commands, function (k, v) {
					var commandId = $(v).attr('class').split('-')[1];
					var command = $(this).find('.command').val();
					var reply = $(this).find('.reply').val();
					// var options = [];

					// $.each($(this).find('.option'), function () {
					// 	if ($(this).val() != '') {
					// 		options.push($(this).val());	
					// 	}
					// });

					commandArray.push({
						'commandId' : commandId,
						'command' : command,
						'reply' : reply,
						// 'options' : options,
					});
				});
				// console.log(commandArray);
				$.post("index.php", {
					updateBot: 'Update',
					commandArray: commandArray,
				}).done(function(data){
					alert(data);
					location.reload();
				});
			});

			$('button[name=add_command]').click(function () {
				var commandId = parseInt($('tr').last().attr('class').split('-')[1]) + 1;
				var table_row = $('<tr class="command-' + commandId + '"/>').appendTo($('#test-table'));
				var command_cell = $('<td>' + '<input type="text" class="command form-control">' + '</td>').appendTo(table_row);
				var reply_cell = $('<td>' + '<input type="text" class="reply form-control">' + '</td>').appendTo(table_row);
			});

			$('body').on('click', 'button[name="delete_command"]', function() {
				var commandId = $(this).attr('command');
				$.post("index.php", {
					deleteCommand: 'Delete',
					commandId: commandId,
				}).done(function(data){
					alert(data);
					location.reload();
				});
			});

			$('#logoutIcon').click(function(){
				$.post("index.php", {
					Logout: 'Logout',
				}).done(function(data){
					location.reload();
				});
			});
			// $('body').on('click', 'button[name="add-option"]', function () {
			// 	var option = $(this).attr('option');
			// 	var new_option = '<li class="list-group-item"><input type="text" class="option form-control" value=""></li>';
			// 	$(this).before(new_option);
			// 
			// });
		});
	</script>
</head>
<body>
	<div class="content">
		<div class="row">
			<div class="panel panel-default">
				<div class="panel-body">
					<span class="glyphicon glyphicon-log-out pull-right" id="logoutIcon"></span>
					<span class="pull-right" id="header-username"></span>
					Admin header
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<nav class="navbar navbar-default" role="navigation">
				<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#">Bot Admin Panel</a>
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse navbar-ex1-collapse">
						<ul class="nav navbar-nav">
							<li class="active"><a href="#">Your First Bot</a></li>
							<li><a href="#">Your Second Bot</a></li>
							
						</ul>
					</div><!-- /.navbar-collapse -->
				</nav>
			</div>
			<div class="col-md-8">
				<div class="row">
					<div class="col-md-6">
						<div class="input-group">
							<span class="input-group-addon">Bot token:</span>
							<input type="text" class="form-control" value="123456789:ABcdfGH1jKlmnopqRsTUVWXyZABcDEfghIj">
							<span class="input-group-addon" id="apply_bot_token">Connect</span>
						</div>
						
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<table class="table table-bordered" id="test-table">
							<tr>
								<th>Command</th>
								<th>Reply</th>
								<th>Delete</th>
								<!-- <th>Option</th> -->
							</tr>
						</table>
						<button class="btn md ghost" name="add_command">Add Command</button>
						<button class="btn md cta" name="submit">Submit</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>