<?php
	function create($db, $table, $fields) {
		// CREATE TABLE Persons
		// (
		// 	PersonID int,
		// 	LastName varchar(255),
		// 	FirstName varchar(255),
		// 	Address varchar(255),
		// 	City varchar(255)
		// );

		$query = 'CREATE TABLE ' . $table . ' (';

		foreach ($fields as $k => $v) {
			$query .= $k . ' ' . $v . ', ';
		}

		$query .= ');';
		die($query);
		$queryResult = $db->query($query);
		return $queryResult;
	}

	function insert($db, $table, $fields, $values) {
		$query = 'INSERT INTO ' . $table . ' (' . $fields . ') VALUES (' . $values . ')';
		// echo $query; die();
		$queryResult = $db->query($query);
		return $queryResult;
	}

	function update($db, $table, $set, $where) {
		$query = 'UPDATE ' . $table . ' SET ' . $set . ' WHERE ' . $where;
		// echo $query; die();
		$queryResult = $db->query($query);
		return $queryResult;
	}

	function select($db, $what, $where, $condition) {
		if (isset($condition)) 
			$query = 'SELECT ' . $what . ' FROM ' . $where . ' WHERE ' . $condition;
		else
			$query = 'SELECT ' . $what . ' FROM ' . $where;
		// echo $query; die();
		$queryResult = $db->query($query);
		$result = array();
		while ($row = $queryResult->fetch_assoc()) {
			array_push($result, $row);
		}
		return $result;
	}

	function delete($db, $from, $where) {
		$query = 'DELETE FROM ' . $from . ' WHERE ' . $where;
		$queryResult = $db->query($query);
		// echo $query; die();
		return $queryResult;
	}

	function crypto_rand($min,$max,$pedantic=True) {
		$diff = $max - $min;
		if ($diff <= 0) return $min; // not so random...
		$range = $diff + 1; // because $max is inclusive
		$bits = ceil(log(($range),2));
		$bytes = ceil($bits/8.0);
		$bits_max = 1 << $bits;
		// e.g. if $range = 3000 (bin: 101110111000)
		//  +--------+--------+
		//  |....1011|10111000|
		//  +--------+--------+
		//  bits=12, bytes=2, bits_max=2^12=4096
		$num = 0;
		do {
			$num = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes))) % $bits_max;
			if ($num >= $range) {
				if ($pedantic) continue; // start over instead of accepting bias
				// else
				$num = $num % $range;  // to hell with security
			}
			break;
		} while (True);  // because goto attracts velociraptors
		return $num + $min;
	}
?>