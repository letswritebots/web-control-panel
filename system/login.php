<?php
	include 'system/functions.php';
	session_start();
	if (isset($_POST['authorize'])) {
		$connectionData = $_SESSION['connectionData'];
		$db = new mysqli($connectionData['HOST'], $connectionData['USER'], $connectionData['PASSWORD'], $connectionData['BASE'], "3306");
		$db->set_charset("utf8");

		$email = $_POST['inputEmail'];
		$password = $_POST['inputPassword'];
		$user = select($db, '*', 'Users', 'Email=\'' . $email. '\' AND Password=\'' . $password . '\'')[0];
		if ($user != []) {
			$auth_token = $user['Auth_token'];
			if ($auth_token == null) {
				$auth_token = strval(md5(crypto_rand(0, 262144)));
				update($db, 'Users', 'Auth_token=\'' . $auth_token . '\'', 'Id=\'' . $user['Id'] . '\'');
			}
			if (isset($_POST['rememberValue']) and $_POST['rememberValue'] == 'remember-me') {
				setcookie("AUTH_TOKEN", $auth_token);				
			}
		}
		$_SESSION['User'] = $user;
		// die(print_r($_SESSION));
		header('Location: /index.html');
		die();
	}
?>

<!-- <!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="icon" href="../../favicon.ico">

		<title>Sign In</title>
		<script src="/template/js/jquery.min.js" type="text/javascript"></script>
		<script src="/template/js/bootstrap.min.js" type="text/javascript"></script>
		<link href="template/css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<link href="template/css/button-style.css" rel="stylesheet" type="text/css">
		<script type="text/javascript">
		$(document).ready(function () {

		})
		</script>
	</head>

	<body>

		<div class="container">
			<div class="row">
				<div class="col-md-4 col-md-offset-4">
					<form class="form-signin" action="login.php" method="post">
						<h2 class="form-signin-heading">Please sign in</h2>
						<input name="authorize" hidden>
						<label for="inputEmail" class="sr-only">Email address</label>
						<input type="email" name="inputEmail" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
						<label for="inputPassword" class="sr-only">Password</label>
						<input type="password" name="inputPassword" id="inputPassword" class="form-control" placeholder="Password" required>
						<div class="checkbox">
							<label>
								<input type="checkbox" name="rememberValue" value="remember-me"> Remember me
							</label>
						</div>
						<button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
					</form>
				</div>
			</div>
		</div> 

		<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
	</body>
</html> -->
