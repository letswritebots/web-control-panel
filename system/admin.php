<?php
	include 'functions.php';
	session_start();

	// $connectionData['HOST'] = '188.120.233.65';
	// $connectionData['USER'] = 'telegram';
	// $connectionData['PASSWORD'] = 'telegram12345';
	// $connectionData['BASE'] = 'phoneAdvisor';

	$connectionData = $_SESSION['connectionData'];
	$db = new mysqli($connectionData['HOST'], $connectionData['USER'], $connectionData['PASSWORD'], 'telegram', "3306");
	$db->set_charset("utf8");


	if (isset($_POST['Logout']) and $_POST['Logout'] != null) {
		unset($_COOKIE['AUTH_TOKEN']);
		$_SESSION['User'] = null;
		header('Location: /login.php');
	}

	if (!isset($_SESSION['User']) and !isset($_COOKIE["AUTH_TOKEN"])) {
		header('Location: /login.php');
		die();
	}

	if (isset($_GET['getBrands'])) {
		$brands = select($db, '*', 'Brands', NULL);
		echo json_encode($brands); die();
	}

	if (isset($_GET['getModels'])) {
		if ($_GET['getModels'] == 'GET') {
			$brands = select($db, '*', 'Brands', NULL);
			$brandsArr = [];
			$modelsArr = [];
			$finalArr = [];
			foreach ($brands as $k => $v) {
				$brandsArr[$v['Id']] = $v['Brand'];
				$models = select($db, '*', 'Models', 'Brand=\'' . $v['Id'] . '\'');
				foreach ($models as $n => $m) {
					$modelsArr[$m['Id']] = ['Brand' => $v['Id'], 'Model' => $m['Model']];
				}
			}
			$finalArr['Brands'] = $brandsArr;
			$finalArr['Models'] = $modelsArr;
			echo json_encode($finalArr); die();
		} else {
			$brand = select($db, 'Id', 'Brands', 'Brand=\'' . $_GET['getModels'] . '\'')[0]['Id'];
			$models = select($db, '*', 'Models', 'Brand=\'' . $brand . '\'');
			echo json_encode($models); die();
		}
	}

	if (isset($_GET['updateUser'])) {
		$user = select($db, '*', 'Users', 'Username=\'' . $_GET['updateUser'] . '\'', NULL);
		if (empty($user)) {
			$username = $_GET['updateUser'];
			$brand = select($db, 'Id', 'Brands', 'Brand=\'' . $_GET['brand'] . '\'')[0]['Id'];
			$model = select($db, 'Id', 'Models', 'Model=\'' . $_GET['model'] . '\'')[0]['Id'];
			insert($db, 'Users', 'Username, Brand, Model', '\'' . $username . '\', \'' . $brand . '\', \'' . $model . '\'');
		} else {
			$username = $user[0]['Username'];
			$brand = select($db, 'Id', 'Brands', 'Brand=\'' . $_GET['brand'] . '\'')[0]['Id'];
			$model = select($db, 'Id', 'Models', 'Model=\'' . $_GET['model'] . '\'')[0]['Id'];
			update($db, 'Users', 'Brand=\'' . $brand . '\'' . ', Model=\'' . $model . '\'', 'Username=\'' . $username . '\'');
		}
		// echo $user; die();
	}

	if (isset($_GET['getCategories'])) {
		$categories = select($db, '*', 'Categories', NULL);
		echo json_encode($categories); die();
	}

	if (isset($_GET['getUsersPhone'])) {
		$phone = array();
		$brand = select($db, 'Brand', 'Users', 'Username=\'' . $_GET['getUsersPhone'] . '\'');
		$model = select($db, 'Model', 'Users', 'Username=\'' . $_GET['getUsersPhone'] . '\'');
		array_push($phone, $brand);
		array_push($phone, $model);
		echo json_encode($phone); die();
	}

	if (isset($_GET['getInstructions']) and $_GET['getInstructions'] == 'GET') {
		$instructions = select($db, '*', 'Instruction', NULL);
		die(json_encode($instructions));
	} else if (isset($_GET['getInstructions'])) {
		$brand = $_GET['brand'];
		$categoryName = $_GET['getInstructions'];
		$category = select($db, 'Id', 'Categories', 'Name=\'' . $categoryName . '\'')[0]['Id'];
		$instructions = array();
		if (isset($_GET['model'])) {
			$model = $_GET['model'];
			$instruction = select($db, 'Link, Header', 'Instruction', 'Brand=\'' . $brand . '\' AND Model=\'' . $model . '\' AND Category=\'' . $category . '\'');
		} else {
			$instruction = select($db, 'Link, Header', 'Instruction', 'Brand=\'' . $brand . '\' AND Category=\'' . $category . '\'');
		}
		foreach ($instruction as $k => $v) {
			$instructions[$v['Header']] = $v['Link'];
		}
		echo json_encode($instructions); die();
	}

	if (isset($_GET['getSongs'])) {
		$songs = select($db, '*', 'Chart', NULL);
		echo json_encode($songs); die();
	}

	if (isset($_POST['likeSong'])) {
		$song = $_POST['likeSong'];
		$username = $_POST['username'];
		update($db, 'Chart', 'Likes = Likes + 1', 'Id=\'' . $song . '\'');
		update($db, 'Users', 'VotedFor = \'' . $song . '\'', 'Username=\'' . $username . '\'');
		die('Thanks for voting');
	}

	if (isset($_GET['showChart'])) {
		$chart = select($db, '*', 'Chart', NULL);
		echo json_encode($chart); die();
	}

	if (isset($_GET['VotedFor'])) {
		$username = $_GET['VotedFor'];
		$vote = select($db, 'VotedFor', 'Users', 'Username=\'' . $username . '\'');
		echo json_encode($vote[0]['VotedFor']); die();
	}

	if (isset($_GET['getDataForApi'])) {
		$data = array();
		$brands = select($db, '*', 'Brands', NULL);
		$models = select($db, '*', 'Models', NULL);
		$categories = select($db, '*', 'Categories', NULL);
		$instructions = select($db, '*', 'Instruction', NULL);

		$data['Brands'] = $brands;
		$data['Models'] = $models;
		$data['Categories'] = $categories;
		$data['Instructions'] = $instructions;
		echo json_encode($data); die();
	}

	if (isset($_POST['updateInstruction']) and $_POST['updateInstruction'] == 'Update') {
		$id = $_POST['id'];
		$url = quotemeta($_POST['url']);
		$header = $_POST['header'];
		$brand = $_POST['brand'];
		$model = $_POST['model'];
		$category = $_POST['category'];

		$isNew = select($db, '*', 'Instruction', 'Id=\''.$id.'\'');

		if ($isNew == []) {
			if ($model == '') {
				insert($db, 'Instruction', 'Id, Link, Header, Brand, Category', '\''.$id.'\', \''.$url.'\', \''.$header.'\', \''.$brand.'\', \''.$category.'\'');
			} else {
				insert($db, 'Instruction', 'Id, Link, Header, Brand, Model, Category', '\''.$id.'\', \''.$url.'\', \''.$header.'\', \''.$brand.'\', \''.$model.'\', \''.$category.'\'');
			}
		} else {
			if ($model == '') {
				update($db, 'Instruction', 'Link=\''.$url.'\', Header=\''.$header.'\', Brand=\''.$brand.'\', Category=\''.$category.'\'', 'Id=\''.$id.'\'');
			} else {
				update($db, 'Instruction', 'Link=\''.$url.'\', Header=\''.$header.'\', Brand=\''.$brand.'\', Model=\''.$model.'\', Category=\''.$category.'\'', 'Id=\''.$id.'\'');
			}
		}

		
		echo 'Success'; die();
	}

	if (isset($_POST['updateInstruction']) and $_POST['updateInstruction'] == 'Delete') {
		$id = $_POST['id'];
		delete($db, 'Instruction', 'Id=\''.$id.'\'');
		die('Success');
	}

	if (isset($_POST['updateChart']) and $_POST['updateChart'] == 'Update') {
		$id = $_POST['id'];
		$singer = $_POST['singer'];
		$song = $_POST['song'];
		$likes = $_POST['likes'];
		
		$isNew = select($db, '*', 'Chart', 'Id=\''.$id.'\'');
		if ($isNew == []) {
			insert($db, 'Chart', 'Id, Singer, Song, Likes', '\''.$id.'\', \''.$singer.'\', \''.$song.'\', \''.$likes.'\'');
		} else {
			update($db, 'Chart', 'Singer=\''.$singer.'\', Song=\''.$song.'\', Likes=\''.$likes.'\'', 'Id=\''.$id.'\'');
		}

		die('Success');
	}

	if (isset($_POST['updateChart']) and $_POST['updateChart'] == 'Delete') {
		$id = $_POST['id'];
		delete($db, 'Chart', 'Id=\''.$id.'\'');
		die('Success');
	}

	if (isset($_POST['updateModel']) and $_POST['updateModel'] == 'Update') {
		$id = $_POST['id'];
		$brand = $_POST['brand'];
		$model = $_POST['model'];

		$isNewBrand = select($db, '*', 'Brands', 'Brand=\''.$brand.'\'');
		if ($isNewBrand == []) {
			insert($db, 'Brands', 'Brand', '\''.$brand.'\'');
		}

		$brand = select($db, 'Id', 'Brands', 'Brand=\'' . $_POST['brand'] . '\'')[0]['Id'];

		$isNewModel = select($db, '*', 'Models', 'Id=\''.$id.'\'');
		if ($isNewModel == []) {
			insert($db, 'Models', 'Id, Brand, Model', '\''.$id.'\', \''.$brand.'\', \''.$model.'\'');
		} else {
			update($db, 'Models', 'Brand=\''.$brand.'\', Model=\''.$model.'\'', 'Id=\''.$id.'\'');
		}

		die('Success');
	}

	if (isset($_POST['updateModel']) and $_POST['updateModel'] == 'Delete') {
		$id = $_POST['id'];
		delete($db, 'Models', 'Id=\''.$id.'\'');
		die('Success');
	}

	if (isset($_GET['getUser']) and $_GET['getUser'] == 'GET') {
		echo json_encode($_SESSION['User']);
		die();
	}

	function getDayById($days, $id) {
		foreach ($days as $k => $v) {
			if ($v['Id'] == $id)
				return $v;
		}
	}

	function getUserById($users, $id) {
		foreach ($users as $k => $v) {
			if ($v['Id'] == $id)
				return $v;
		}
	}

	// function insert($db, $table, $fields, $values) {
	// 	$query = 'INSERT INTO ' . $table . ' (' . $fields . ') VALUES (' . $values . ')';
	// 	// echo $query; die();
	// 	$queryResult = $db->query($query);
	// 	return $queryResult;
	// }

	// function update($db, $table, $set, $where) {
	// 	$query = 'UPDATE ' . $table . ' SET ' . $set . ' WHERE ' . $where;
	// 	echo $query; die();
	// 	$queryResult = $db->query($query);
	// 	return $queryResult;
	// }

	// function select($db, $what, $where, $condition) {
	// 	if (isset($condition)) 
	// 		$query = 'SELECT ' . $what . ' FROM ' . $where . ' WHERE ' . $condition;
	// 	else
	// 		$query = 'SELECT ' . $what . ' FROM ' . $where;
	// 	// echo $query; die();
	// 	$queryResult = $db->query($query);
	// 	$result = array();
	// 	while ($row = $queryResult->fetch_assoc()) {
	// 		array_push($result, $row);
	// 	}
	// 	return $result;
	// }

	// function delete($db, $from, $where) {
	// 	$query = 'DELETE FROM ' . $from . ' WHERE ' . $where;
	// 	$queryResult = $db->query($query);
	// 	return $queryResult;
	// }
?>

<!DOCTYPE html>
<html>
<head>
	<title>Telegram Bot API</title>
	<!-- Bootstrap Toggle -->
	
	<link href="/template/css/bootstrap.css" rel="stylesheet">
	<link href="/template/css/bootstrap-switch.css" rel="stylesheet">
	<script src="/template/js/jquery.js"></script>
	<script src="/template/js/bootstrap-switch.js"></script>
	
	<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
	<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
	
	<!-- /Bootstrap Toggle -->
	
	<script src="/template/js/jquery.min.js" type="text/javascript"></script>
	<script src="/template/js/bootstrap.min.js" type="text/javascript"></script>
	<link href="/template/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="/template/css/button-style.css" rel="stylesheet" type="text/css">
	
	<style type="text/css">
		body {
			margin-left: 20px;
			margin-right: 20px;
		}

		@media (min-width: 768px) {
			.navbar-collapse {
				height: auto;
				border-top: 0;
				box-shadow: none;
				max-height: none;
				padding-left:0;
				padding-right:0;
			}
			.navbar-collapse.collapse {
				display: block !important;
				width: auto !important;
				padding-bottom: 0;
				overflow: visible !important;
			}
			.navbar-collapse.in {
				overflow-x: visible;
			}

			.navbar
			{
				max-width:300px;
				margin-right: 0;
				margin-left: 0;
			}	

			.navbar-nav,
			.navbar-nav > li,
			.navbar-left,
			.navbar-right,
			.navbar-header
			{float:none !important;}

			.navbar-right .dropdown-menu {left:0;right:auto;}
			.navbar-collapse .navbar-nav.navbar-right:last-child {
				margin-right: 0;
			}

			#logoutIcon {
				cursor: pointer;
			}
		}
	</style>
	<script type="text/javascript">
		$(document).ready(function(){
			// fill models table
			var userQuery = $.getJSON("admin.php/?getUser=GET", function(data) {
				$('#header-username').text(data['Username']).append('&nbsp;&nbsp;&nbsp;');
			});

			var modelsQuery = $.getJSON("admin.php/?getModels=GET", function(data) {
				var brands = [];
				var models = [];

				$.each(data['Brands'], function(k, v) {
					brands[k] = v;
				});

				// console.log(brands);

				$.each(data['Models'], function(k, v) {
					var id = k;
					var table_row = $('<tr class="model-' + id + '"/>').appendTo($('#models_table'));
					var id_cell = $('<td class="model_id" id="' + id + '">' + id + '</td>').appendTo(table_row);
					var brandsSelector = $('<select class="form-control model-' + id + '" id="brands_selector"/>');
					$.each(brands, function(id, name) {
						if (id == v['Brand']) {
							$('<option value="' + id + '" selected>' + name + '</option>').appendTo(brandsSelector);
						} else if (name != undefined) {
							$('<option value="' + id + '">' + name + '</option>').appendTo(brandsSelector);
						}
					});
					var brandsCell = $('<td></td>');
					brandsSelector.appendTo(brandsCell);
					brandsCell.appendTo(table_row);

					var modelsSelector = $('<select class="form-control model-' + id + '" id="models_selector"/>');
					$.each(data['Models'], function(m, name) {
						if (m == id) {
							$('<option value="' + m + '" selected>' + name['Model'] + '</option>').appendTo(modelsSelector);
						} else {
							$('<option value="' + m + '">' + name['Model'] + '</option>').appendTo(modelsSelector);
						}
					});
					var modelsCell = $('<td></td>');
					modelsSelector.appendTo(modelsCell);
					modelsCell.appendTo(table_row);

					var edit_btn = '<button type="button" class="btn btn-default" name="edit_model" model="' + id + '"><span class="glyphicon glyphicon-pencil"></span></button>';
					var accept_btn = '<button type="button" class="btn btn-default" name="accept_model" model="' + id + '"><span class="glyphicon glyphicon-ok"></span></button>';
					var delete_btn = '<button type="button" class="btn btn-default" name="delete_model" model="' + id + '"><span class="glyphicon glyphicon-remove"></span></button>';

					var edit_btn_cell = $('<td></td>').appendTo(table_row);
					$(edit_btn).appendTo(edit_btn_cell);

					var accept_btn_cell = $('<td></td>').appendTo(table_row);
					$(accept_btn).appendTo(accept_btn_cell);

					var delete_btn_cell = $('<td></td>').appendTo(table_row);
					$(delete_btn).appendTo(delete_btn_cell);
				});
			});

			// fill instructions table
			var instructionsQuery = $.getJSON("admin.php/?getDataForApi=GET")
				.done(function(data) {

					var brands = data['Brands'];
					var models = data['Models'];
					var categories = data['Categories'];
					var instructions = data['Instructions'];
					// alert(JSON.stringify(instructions));
					
					for (var i = 0; i < instructions.length; i++) {
						var id = instructions[i]['Id'];
						var url = instructions[i]['Link'];
						var header = instructions[i]['Header'];

						var brandId = instructions[i]['Brand'];
						var modelId = instructions[i]['Model'];
						var categoryId = instructions[i]['Category'];

						// creating selectors for brands, models and categories
						var brandsSelector = $('<select class="form-control instruction-' + id + '" id="brands_selector" disabled/>');
						for (var j = 0; j < brands.length; j++) {
							if (brandId == brands[j]['Id']) {
								$('<option value="' + brands[j]['Id'] + '" selected>' + brands[j]['Brand'] + '</option>').appendTo(brandsSelector);
							} else {
								$('<option value="' + brands[j]['Id'] + '">' + brands[j]['Brand'] + '</option>').appendTo(brandsSelector);
							}
						}

						var modelsSelector = $('<select class="form-control instruction-' + id + '" id="models_selector" disabled/>');
						if (modelId == null) {
							$("<option selected/>").appendTo(modelsSelector);
						} else {
							$("<option />").appendTo(modelsSelector);
						}
						for (var j = 0; j < models.length; j++) {
							if (modelId == models[j]['Id']) {
								$("<option selected/>", {value: models[j]['Id'], text: models[j]['Model']}).appendTo(modelsSelector);
							} else {
								$("<option />", {value: models[j]['Id'], text: models[j]['Model']}).appendTo(modelsSelector);
							}
						}

						var categoriesSelector = $('<select class="form-control instruction-' + id + '" id="categories_selector" disabled/>');
						for (var j = 0; j < categories.length; j++) {
							if (categoryId == categories[j]['Id']) {
								$('<option value="' + categories[j]['Id'] + '" selected>' + categories[j]['Name'] + '</option>').appendTo(categoriesSelector);
							} else {
								$('<option value="' + categories[j]['Id'] + '">' + categories[j]['Name'] + '</option>').appendTo(categoriesSelector);
							}
						}
						// end
						var edit_btn = '<button type="button" class="btn btn-default" name="edit_instruction" instruction="' + id + '"><span class="glyphicon glyphicon-pencil"></span></button>';
						var accept_btn = '<button type="button" class="btn btn-default" name="accept_instruction" instruction="' + id + '"><span class="glyphicon glyphicon-ok"></span></button>';
						var delete_btn = '<button type="button" class="btn btn-default" name="delete_instruction" instruction="' + id + '"><span class="glyphicon glyphicon-remove"></span></button>';

						var table_row = $('<tr class="instruction-' + id + '"/>').appendTo($('#instructions_table'));
						var id_cell = $('<td class="instruction_id" id="' + id + '">' + id + '</td>').appendTo(table_row);
						var url_cell = $('<td></td>').appendTo(table_row);
						var url_edit = $('<input type="text" class="form-control instruction-' + id + '" value="' + url + '" field="url" disabled>').appendTo(url_cell);
						var header_cell = $('<td></td>').appendTo(table_row);
						var header_edit = $('<input type="text" class="form-control instruction-' + id + '" value="' + header + '" field="header" disabled>').appendTo(header_cell);

						var brands_cell = $('<td></td>').appendTo(table_row);
						brandsSelector.appendTo(brands_cell);

						var models_cell = $('<td></td>').appendTo(table_row);
						modelsSelector.appendTo(models_cell);

						var categories_cell = $('<td></td>').appendTo(table_row);
						categoriesSelector.appendTo(categories_cell);

						var edit_btn_cell = $('<td></td>').appendTo(table_row);
						$(edit_btn).appendTo(edit_btn_cell);

						var accept_btn_cell = $('<td></td>').appendTo(table_row);
						$(accept_btn).appendTo(accept_btn_cell);

						var delete_btn_cell = $('<td></td>').appendTo(table_row);
						$(delete_btn).appendTo(delete_btn_cell);

					}
				}).fail(function() {
					alert("error");
			});

			// fill chart table
			var chartQuery = $.getJSON("admin.php/?showChart=GET")
				.done(function(data) {
					$.each(data, function(key, value) {
						var id = value['Id'];
						var singer = '<input type="text" class="form-control song-' + id + '" value="' + value['Singer'] + '" field="singer" disabled>';
						var song = '<input type="text" class="form-control song-' + id + '" value="' + value['Song'] + '" field="song" disabled>';
						var likes = '<input type="text" class="form-control song-' + id + '" value="' + value['Likes'] + '" field="likes" disabled>';

						var edit_btn = '<button type="button" class="btn btn-default" name="edit_song" song="' + id + '"><span class="glyphicon glyphicon-pencil"></span></button>';
						var accept_btn = '<button type="button" class="btn btn-default" name="accept_song" song="' + id + '"><span class="glyphicon glyphicon-ok"></span></button>';
						var delete_btn = '<button type="button" class="btn btn-default" name="delete_song" song="' + id + '"><span class="glyphicon glyphicon-remove"></span></button>';

						var table_row = '<tr class="song-' + id + '">' + 
						'<td class="song_id" id="' + id + '">' + id + '</td>' + 
						'<td>' + singer + '</td>' + 
						'<td>' + song + '</td>' + 
						'<td>' + likes + '</td>' + 
						'<td>' + edit_btn + '</td>' + 
						'<td>' + accept_btn + '</td>' + 
						'<td>' + delete_btn + '</td>' + 
						'</tr>';
						$('#chart_table').append(table_row);
					});
				});

			$('body').on('click', 'button[name="delete_instruction"]', function() { 
				var id = $(this).attr('instruction');
				$.post("admin.php", {
					updateInstruction: 'Delete',
					id: id,
				}).done(function(data){
					alert(data);
					location.reload();
				});

			});

			$('body').on('click', 'button[name="delete_song"]', function() { 
				var id = $(this).attr('song');
				$.post("admin.php", {
					updateChart: 'Delete',
					id: id,
				}).done(function(data){
					alert(data);
					location.reload();
				});
			});
			$('body').on('click', 'button[name="edit_instruction"]', function() { 
				var instruction = $(this).attr('instruction');
				$('.instruction-' + instruction).prop('disabled', function(i, v) { return !v; });
			});

			$('body').on('click', 'button[name="edit_song"]', function() { 
				var song = $(this).attr('song');
				$('.song-' + song).prop('disabled', function(i, v) { return !v; });
			});

			$('body').on('click', 'button[name="accept_instruction"]', function() { 
				var id = $(this).attr('instruction');
				var url = $('.instruction-' + id + '[field="url"]').val();
				var header = $('.instruction-' + id + '[field="header"]').val();
				var brand = $('.instruction-' + id + '[id="brands_selector"]').val();
				var model = $('.instruction-' + id + '[id="models_selector"]').val();
				var category = $('.instruction-' + id + '[id="categories_selector"]').val();

				$.post("admin.php", {
					updateInstruction: 'Update',
					id: id,
					url: url,
					header: header,
					brand: brand,
					model: model,
					category: category,
				}).done(function(data){
					alert(data);
					location.reload();
				});
			});

			$('body').on('click', 'button[name="accept_song"]', function() { 
				var id = $(this).attr('song');
				var singer = $('.song-' + id + '[field="singer"]').val();
				var song = $('.song-' + id + '[field="song"]').val();
				var likes = $('.song-' + id + '[field="likes"]').val();

				$.post("admin.php", {
					updateChart: 'Update',
					id: id,
					singer: singer,
					song: song,
					likes: likes,
				}).done(function(data){
					console.log(data);
					alert(data);
					location.reload();
				});
			});

			$('body').on('click', 'button[name="accept_model"]', function() { 
				var id = $(this).attr('model');
				var brand = $('.model-' + id + '[id="brands_selector_add"]').val();
				var model = $('.model-' + id + '[id="models_selector_add"]').val();

				if (brand == undefined) {
					brand = $('.model-' + id + '[id="brands_selector"]').val();
				}

				if (model == undefined) {
					model = $('.model-' + id + '[id="models_selector"]').val();
				}

				$.post("admin.php", {
					updateModel: 'Update',
					id: id,
					brand: brand,
					model: model,
				}).done(function(data){
					alert(data);
					location.reload();
				});
			});

			$('body').on('click', 'button[name="edit_model"]', function() { 
				var id = $(this).attr('model');
				var brand = $('.model-' + id + '[id=brands_selector] option:selected').text();
				var model = $('.model-' + id + '[id=models_selector] option:selected').text();

				$('.model-' + id + '[id=brands_selector]').replaceWith('<input type="text" class="form-control model-' + id + '" id="brands_selector_add" value="' + brand + '">');
				$('.model-' + id + '[id=models_selector]').replaceWith('<input type="text" class="form-control model-' + id + '" id="models_selector_add" value="' + model + '">');
			});

			$('body').on('click', 'button[name="delete_model"]', function() { 
				var id = $(this).attr('model');
				$.post("admin.php", {
					updateModel: 'Delete',
					id: id,
				}).done(function(data){
					alert(data);
					location.reload();
				});
			});

			$('#add_model_row').click(function() {
				var id = parseInt($('.model_id:last').attr('id')) + 1;
				var modelsQuery = $.getJSON("index.php/?getModels=GET", function(data) {
					var brands = [];
					var models = [];

					$.each(data['Brands'], function(k, v) {
						brands[k] = v;
					});

					// console.log(brands);

					var table_row = $('<tr class="model-' + id + '"/>').appendTo($('#models_table'));
					var id_cell = $('<td class="model_id" id="' + id + '">' + id + '</td>').appendTo(table_row);
					
					var brand_cell = $('<td></td>').appendTo(table_row);
					var brand_name = $('<input type="text" class="form-control model-' + id + '" id="brands_selector_add">');
					brand_name.appendTo(brand_cell);

					var model_cell = $('<td></td>').appendTo(table_row);
					var model_name = $('<input type="text" class="form-control model-' + id + '" id="models_selector_add">');
					model_name.appendTo(model_cell);

					var edit_btn = '<button type="button" class="btn btn-default" name="edit_model" model="' + id + '"><span class="glyphicon glyphicon-pencil"></span></button>';
					var accept_btn = '<button type="button" class="btn btn-default" name="accept_model" model="' + id + '"><span class="glyphicon glyphicon-ok"></span></button>';
					var delete_btn = '<button type="button" class="btn btn-default" name="delete_model" model="' + id + '"><span class="glyphicon glyphicon-remove"></span></button>';

					var edit_btn_cell = $('<td></td>').appendTo(table_row);
					$(edit_btn).appendTo(edit_btn_cell);

					var accept_btn_cell = $('<td></td>').appendTo(table_row);
					$(accept_btn).appendTo(accept_btn_cell);

					var delete_btn_cell = $('<td></td>').appendTo(table_row);
					$(delete_btn).appendTo(delete_btn_cell);
				});
			});

			$('#add_instruction_row').click(function(){
				var id = parseInt($('.instruction_id:last').attr('id')) + 1;

				var instructionsQuery = $.getJSON("index.php/?getDataForApi=GET")
					.done(function(data) {
						var brands = data['Brands'];
						var models = data['Models'];
						var categories = data['Categories'];

						// creating selectors for brands, models and categories
						var brandsSelector = $('<select class="form-control instruction-' + id + '" id="brands_selector"/>');
						$('<option selected/>').appendTo(brandsSelector);
						for (var j = 0; j < brands.length; j++) {
							$('<option value="' + brands[j]['Id'] + '">' + brands[j]['Brand'] + '</option>').appendTo(brandsSelector);
						}

						var modelsSelector = $('<select class="form-control instruction-' + id + '" id="models_selector"/>');
						$('<option selected/>').appendTo(modelsSelector);
						for (var j = 0; j < models.length; j++) {
							$("<option />", {value: models[j]['Id'], text: models[j]['Model']}).appendTo(modelsSelector);
						}

						var categoriesSelector = $('<select class="form-control instruction-' + id + '" id="categories_selector"/>');
						$('<option selected/>').appendTo(categoriesSelector);
						for (var j = 0; j < categories.length; j++) {
							$('<option value="' + categories[j]['Id'] + '">' + categories[j]['Name'] + '</option>').appendTo(categoriesSelector);
						}


						var edit_btn = '<button type="button" class="btn btn-default" name="edit_instruction" instruction="' + id + '"><span class="glyphicon glyphicon-pencil"></span></button>';
						var accept_btn = '<button type="button" class="btn btn-default" name="accept_instruction" instruction="' + id + '"><span class="glyphicon glyphicon-ok"></span></button>';
						var delete_btn = '<button type="button" class="btn btn-default" name="delete_instruction" instruction="' + id + '"><span class="glyphicon glyphicon-remove"></span></button>';
						// end

						var table_row = $('<tr class="instruction-' + id + '"/>').appendTo($('#instructions_table'));
						var id_cell = $('<td class="instruction_id" id="' + id + '">' + id + '</td>').appendTo(table_row);
						var url_cell = $('<td></td>').appendTo(table_row);
						var url_edit = $('<input type="text" class="form-control instruction-' + id + '" value="" field="url">').appendTo(url_cell);
						var header_cell = $('<td></td>').appendTo(table_row);
						var header_edit = $('<input type="text" class="form-control instruction-' + id + '" value="" field="header">').appendTo(header_cell);

						var brands_cell = $('<td></td>').appendTo(table_row);
						brandsSelector.appendTo(brands_cell);

						var models_cell = $('<td></td>').appendTo(table_row);
						modelsSelector.appendTo(models_cell);

						var categories_cell = $('<td></td>').appendTo(table_row);
						categoriesSelector.appendTo(categories_cell);

						var edit_btn_cell = $('<td></td>').appendTo(table_row);
						$(edit_btn).appendTo(edit_btn_cell);

						var accept_btn_cell = $('<td></td>').appendTo(table_row);
						$(accept_btn).appendTo(accept_btn_cell);

						var delete_btn_cell = $('<td></td>').appendTo(table_row);
						$(delete_btn).appendTo(delete_btn_cell);						
					});
			});

			$('#add_chart_row').click(function(){
				var id = parseInt($('.song_id:last').attr('id')) + 1;
				var singer = '<input type="text" class="form-control song-' + id + '" field="singer">';
				var song = '<input type="text" class="form-control song-' + id + '" field="song">';
				var likes = '<input type="text" class="form-control song-' + id + '" field="likes">';

				var edit_btn = '<button type="button" class="btn btn-default" name="edit_song" song="' + id + '"><span class="glyphicon glyphicon-pencil"></span></button>';
				var accept_btn = '<button type="button" class="btn btn-default" name="accept_song" song="' + id + '"><span class="glyphicon glyphicon-ok"></span></button>';
				var delete_btn = '<button type="button" class="btn btn-default" name="delete_song" song="' + id + '"><span class="glyphicon glyphicon-remove"></span></button>';

				var table_row = '<tr class="song-' + id + '">' + 
				'<td>' + id + '</td>' + 
				'<td>' + singer + '</td>' + 
				'<td>' + song + '</td>' + 
				'<td>' + likes + '</td>' + 
				'<td>' + edit_btn + '</td>' + 
				'<td>' + accept_btn + '</td>' + 
				'<td>' + delete_btn + '</td>' + 
				'</tr>';
				$('#chart_table').append(table_row);
			});

			$("#showModels").click(function(){
				$('#models_content').show();
				$('#chart_content').hide();
				$('#instructions_content').hide();

				$("#showModels").parent().addClass('active');
				$("#showChart").parent().removeClass('active');
				$("#showInstructions").parent().removeClass('active');

			});

			$("#showChart").click(function(){
				$('#models_content').hide();
				$('#chart_content').show();
				$('#instructions_content').hide();

				$("#showModels").parent().removeClass('active');
				$("#showChart").parent().addClass('active');
				$("#showInstructions").parent().removeClass('active');
			});

			$("#showInstructions").click(function(){
				$('#models_content').hide();
				$('#chart_content').hide();
				$('#instructions_content').show();

				$("#showModels").parent().removeClass('active');
				$("#showChart").parent().removeClass('active');
				$("#showInstructions").parent().addClass('active');
			});

			$('#logoutIcon').click(function(){
				$.post("admin.php", {
					Logout: 'Logout',
				}).done(function(data){
					location.reload();
				});
			});
		});
	</script>
</head>
<body>
	<div class="content">
		<div class="row">
			<div class="panel panel-default">
				<div class="panel-body">
					<span class="glyphicon glyphicon-log-out pull-right" id="logoutIcon"></span>
					<span class="pull-right" id="header-username"></span>
					Admin header
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<nav class="navbar navbar-default" role="navigation">
				<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#">Bot Admin Panel</a>
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse navbar-ex1-collapse">
						<ul class="nav navbar-nav" id="navigationMenu">
							<li class="active"><a href="#" id="showModels">Models</a></li>
							<li><a href="#" id="showInstructions">Instructions</a></li>
							<li><a href="#" id="showChart">Chart</a></li>
						</ul>
				</nav>
			</div>
			<div class="col-md-8">
				<div class="row">
					<div class="col-md-12" class="editable_content" id="models_content">
						<div class="row">
							<h3><span class="label label-default">Models</span></h3>
						</div>
						<table class="table table text-center" id="models_table">
							<tr>
								<th>Id</th>
								<th>Brand</th>
								<th>Model</th>
								<th>Edit</th>
								<th>Apply</th>
								<th>Delete</th>
							</tr>
						</table>
						<button class="btn md ghost" id="add_model_row">Add Model</button>
						<!-- <button type="button" class="btn btn-primary" id="add_model_row"><span class="glyphicon glyphicon-plus"></span></button> -->
					</div>
				</div>
				<div class="row">
					<div class="col-md-12" class="editable_content" id="instructions_content" hidden>
						<div class="row">
							<h3><span class="label label-default">Instructions</span></h3>
						</div>
						<table class="table table text-center" id="instructions_table">
							<tr>
								<th>Id</th>
								<th>URL</th>
								<th>Header</th>
								<th>Brand</th>
								<th>Model</th>
								<th>Category</th>
								<th>Edit</th>
								<th>Apply</th>
								<th>Delete</th>
							</tr>
						</table>
						<button class="btn md ghost" id="add_instruction_row">Add Model</button>
						<!-- <button type="button" class="btn btn-primary" id="add_instruction_row"><span class="glyphicon glyphicon-plus"></span></button> -->
					</div>
				</div>
				<div class="row">
					<div class="col-md-12" class="editable_content" id="chart_content" hidden>
						<div class="row">
							<h3><span class="label label-default">Chart</span></h3>
							
						</div>
						<table class="table table" id="chart_table">
							<tr>
								<th>Id</th>
								<th>Singer</th>
								<th>Song</th>
								<th>Likes</th>
								<th>Edit</th>
								<th>Apply</th>
								<th>Delete</th>
							</tr>
						</table>
						<button class="btn md ghost" id="add_chart_row">Add Song</button>
						<!-- <button type="button" class="btn btn-primary" id="add_chart_row"><span class="glyphicon glyphicon-plus"></span></button> -->
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>